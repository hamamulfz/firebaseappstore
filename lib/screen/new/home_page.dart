import 'package:ant_icons/ant_icons.dart';
import 'package:flutter/material.dart';
import 'package:storewarehouse/screen/new/search_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(AntIcons.scan),
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) => SearchPage()));
        },
      ),
      body: SafeArea(
        child: Container(),
      ),
    );
  }
}
