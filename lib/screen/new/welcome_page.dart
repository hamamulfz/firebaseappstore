import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:storewarehouse/screen/new/login_page.dart';
import 'package:storewarehouse/screen/new/register_page.dart';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: <Color>[
                Colors.white,
                Color(0xffF5F6F8),
              ],
              end: Alignment.bottomCenter,
              begin: Alignment.topCenter,
            ),
          ),
          padding: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.05),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Spacer(flex: 3),
              Text(
                'Welcome to',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  color: Colors.grey,
                ),
              ),
              Spacer(flex: 1),
              Text('Jako',
                  style: TextStyle(
                    fontFamily: 'Pacifico',
                    fontSize: 60,
                    color: Color(0xff4997f5),
                  )),
              Spacer(flex: 3),
              InkWell(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => LoginPage()));
                },
                child: Container(
                  width: 235,
                  height: 45,
                  decoration: BoxDecoration(
                      color: Color(0xff4997f5),
                      borderRadius: BorderRadius.circular(20)),
                  child: Center(
                    child: Text(
                      'Log In',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
              Spacer(flex: 2),
              Row(
                children: <Widget>[
                  Expanded(child: Divider(thickness: 1)),
                  SizedBox(width: 15),
                  Text('or login with'),
                  SizedBox(width: 15),
                  Expanded(child: Divider(thickness: 1)),
                ],
              ),
              Spacer(flex: 2),
              Container(
                width: 235,
                height: 45,
                decoration: BoxDecoration(
                    color: Color(0xff3f72af),
                    borderRadius: BorderRadius.circular(20)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        'f',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.lato(
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Text(
                        'Facebook',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Text(''),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20),
              Container(
                width: 235,
                height: 45,
                decoration: BoxDecoration(
                    color: Color(0xffffffff),
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(0, 4),
                        blurRadius: 20,
                        color: Colors.black.withOpacity(0.1),
                      )
                    ]),
                child: Row(
                  children: <Widget>[
                    Expanded(child: Image.asset('assets/google-logo.png')),
                    Expanded(
                      flex: 2,
                      child: Text(
                        'Google',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Text(''),
                    ),
                  ],
                ),
              ),
              Spacer(flex: 1),
              RichText(
                text: TextSpan(
                    style: TextStyle(
                      fontSize: 12,
                    ),
                    children: [
                      TextSpan(
                        text: 'By logging in or registering, I agree to our ',
                        style: TextStyle(color: Colors.grey),
                      ),
                      TextSpan(
                        text: 'Term and Services',
                        style: TextStyle(color: Colors.blue),
                      ),
                      TextSpan(
                        text: ' and ',
                        style: TextStyle(color: Colors.grey),
                      ),
                      TextSpan(
                        text: 'Privacy Policy',
                        style: TextStyle(color: Colors.blue),
                      ),
                    ]),
              ),
              Spacer(flex: 1),
              GestureDetector(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => RegisterPage()));
                },
                child: RichText(
                  text: TextSpan(
                      recognizer: TapGestureRecognizer()
                        ..onTap = () => print('test'),
                      // Navigator.of(context).push(
                      //     MaterialPageRoute(
                      //         builder: (BuildContext context) =>
                      //             RegisterPage())),
                      style: TextStyle(
                        fontSize: 14,
                      ),
                      children: [
                        TextSpan(
                          text: 'Don\'t have an account? ',
                          style: TextStyle(color: Colors.grey),
                        ),
                        TextSpan(
                          text: 'Sign up',
                          style: TextStyle(color: Colors.blue),
                          // recognizer: TapGestureRecognizer()
                          //   ..onTap = () {
                          //     Navigator.of(context).push(MaterialPageRoute(
                          //         builder: (BuildContext context) =>
                          //             RegisterPage()));
                          // },
                        ),
                      ]),
                ),
              ),
              Spacer(flex: 1),
            ],
          ),
        ),
      ),
    );
  }
}
