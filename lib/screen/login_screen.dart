import 'dart:async';
import 'dart:ui';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:storewarehouse/screen/my_home_page.dart';
import 'package:storewarehouse/services/auth.dart';
import 'package:provider/provider.dart';
import 'package:storewarehouse/services/check_connectivity.dart';

class LoginScreen extends StatefulWidget {
  static String route = 'login_screen';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

enum EmailSignInFormType { signIn, register }

class _LoginScreenState extends State<LoginScreen> with WidgetsBindingObserver {
  Timer _timer;
  double _sigmaX = 3.0; // from 0-10
  double _sigmaY = 3.0; // from 0-10
  double _opacity = 0.1; // from 0-1.0

  TextEditingController _phoneController = TextEditingController();
  TextEditingController _otpController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  bool isTypeOtp = false;
  bool _isShowResendOtpButton = false;
  bool isForgotPassword = false;
  String userPhoneNumber = '';
  int _selectedIndex = 0;
  int countDownResendOtp = 15;

  final _phoneFocusNode = FocusNode();
  final _passwordFocusNode = FocusNode();
  // String get _phone => _phoneController.text;
  // String get _otp => _otpController.text;
  // EmailSignInFormType _formType = EmailSignInFormType.signIn;

  PageController _controller = PageController(
    initialPage: 0,
  );

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      _controller.animateToPage(
        _selectedIndex,
        duration: new Duration(milliseconds: 100),
        curve: Curves.easeIn,
      );
    });
  }

  // _loginWithGoogle() async {}
  // _loginWithFacebook() async {}
  // _loginWithEmail() async {}
  // _loginWithTwitter() async {}
  // _loginWithPhone() async {}

  _showInformationSnackbar(String message) {
    final snackBar = SnackBar(
      content: Text(message),
      action: SnackBarAction(
        label: 'OK',
        onPressed: () {
          _scaffoldKey.currentState.hideCurrentSnackBar();
          // Some code to undo the change.
        },
      ),
    );

    // Find the Scaffold in the widget tree and use
    // it to show a SnackBar.
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  _checkDataBeforeNavigate(user) async {
    // final user = Provider.of<Auth>(context).user;
    print('usrnm : ' + user.displayName.toString());
    // if (user.displayName.isEmpty || user.displayName == null) {
    // } else {
    Navigator.pushReplacementNamed(
      context,
      MyHomePage.route,
    );
    // }
  }

  setIsLogin() async {
    final _prefs = await SharedPreferences.getInstance();
    await _prefs.setBool('isFromLogin', true);

    bool isFromLogin = _prefs.getBool('isFromLogin');
    print('from login $isFromLogin');
  }

  FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  String _smsVerificationCode = "";

  verifyPhoneNumber(
      BuildContext context, String countryCode, String number) async {
    // print(countryCode + number);
    print("running");

    final auth = Provider.of<Auth>(context, listen: false);
    auth.setLoadingValue(true);

    _firebaseAuth.setLanguageCode('id');
    String phoneNumber = countryCode + number;
    try {
      await _firebaseAuth.verifyPhoneNumber(
          phoneNumber: phoneNumber,
          timeout: Duration(seconds: 10),
          verificationCompleted: (authCredential) =>
              _verificationComplete(authCredential, context),
          verificationFailed: (authException) =>
              _verificationFailed(authException, context),
          codeAutoRetrievalTimeout: (verificationId) =>
              _codeAutoRetrievalTimeout(verificationId),
          // called when the SMS code is sent
          codeSent: (verificationId, [code]) =>
              _smsCodeSent(verificationId, [code]));
      print(phoneNumber);
    } catch (e) {
      print(e);
    } finally {
      auth.setLoadingValue(false);
    }
  }

  senOtpManually(String code) async {
    final auth = Provider.of<Auth>(context, listen: false);
    try {
      auth.setLoadingValue(true);

      final AuthCredential credential = PhoneAuthProvider.getCredential(
          smsCode: code, verificationId: _smsVerificationCode);
      final FirebaseUser user =
          (await _firebaseAuth.signInWithCredential(credential)).user;
      print(user);
      if (user != null) {
        auth.setUser(user);
        Navigator.pushReplacementNamed(
          context,
          MyHomePage.route,
        );
      }
      return user;
    } catch (e) {
      auth.setLoadingValue(false);
      _showInformationSnackbar(e.message);
      setState(() {});
      print(e.message.toString());
    } finally {
      auth.setLoadingValue(false);
    }
  }

  _verificationComplete(AuthCredential authCredential, BuildContext context) {
    final auth = Provider.of<Auth>(context, listen: false);
    auth.setLoadingValue(true);
    FirebaseAuth.instance
        .signInWithCredential(authCredential)
        .then((authResult) {
      // final snackBar =
      //     SnackBar(content: Text("Success!!! UUID is: " + authResult.user.uid));
      // if (authResult.user != null) {
      auth.setUser(authResult.user);
      Navigator.pushReplacementNamed(
        context,
        MyHomePage.route,
      );
      // }

      // _scaffoldKey.currentState.showSnackBar(snackBar);
      // print("Completed");
      auth.setLoadingValue(false);
      // _sendSignInNotification(authResult.user);
      return authResult.user;
    });
  }

  _smsCodeSent(String verificationId, List<int> code) {
    // set the verification code so that we can use it to log the user in
    _smsVerificationCode = verificationId;

    final snackBar = SnackBar(
      content: Text('Kode dikirim ke +93${_phoneController.text}'),
      action: SnackBarAction(
        label: 'OK',
        onPressed: () {
          _scaffoldKey.currentState.hideCurrentSnackBar();
          // Some code to undo the change.
        },
      ),
    );

    // Find the Scaffold in the widget tree and use
    // it to show a SnackBar.
    _scaffoldKey.currentState.showSnackBar(snackBar);
    print("Send + $verificationId");
  }

  String status = '';

  _verificationFailed(AuthException authException, BuildContext context) {
    print("Failed");
    status = '${authException.message}';
    print("Error message: " + status);
    if (authException.message.contains('not authorized'))
      status = 'Something has gone wrong, please try later';
    else if (authException.message.contains('Network'))
      status = 'Please check your internet connection and try again';
    else
      status = 'Something has gone wrong, please try later';
    // });
    final snackBar = SnackBar(
        content:
            Text("Exception!! message:" + authException.message.toString()));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  _codeAutoRetrievalTimeout(String verificationId) {
    print("Time out");
    // set the verification code so that we can use it to log the user in
    _smsVerificationCode = verificationId;
  }

  _showErrorSnackBar(e) {
    final snackBar = SnackBar(
      content: Text(e.toString()),
      action: SnackBarAction(
        label: 'OK',
        onPressed: () {
          _scaffoldKey.currentState.hideCurrentSnackBar();
          // Some code to undo the change.
        },
      ),
    );

    // Find the Scaffold in the widget tree and use
    // it to show a SnackBar.
    _scaffoldKey.currentState.showSnackBar(snackBar);
    return;
  }

  @override
  void dispose() {
    _phoneController.dispose();
    _otpController.dispose();
    _phoneFocusNode.dispose();
    _passwordFocusNode.dispose();
    _timer.cancel();
    _controller.dispose();

    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    setIsLogin();

    final auth = Provider.of<Auth>(context, listen: false);
    auth.setLoadingValue(false);
    WidgetsBinding.instance.addObserver(this);
    initDynamicLinks();
  }

  Future<void> sendSignInWithEmailLink(String email) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('emailLink', email);

    String counter = prefs.getString('emailLink');
    print(counter + ' is current emaillink');
    if (_emailController.text.isEmpty) return;
    return _firebaseAuth.sendSignInWithEmailLink(
      email: email,
      url: 'https://storewarehouse.page.link/3N7P',
      androidInstallIfNotAvailable: true,
      androidMinimumVersion: '21',
      androidPackageName: 'com.fazawork.storewarehouse',
      handleCodeInApp: true,
      iOSBundleID: 'com.fazawork.storewarehouse',
    );
  }

  Future<AuthResult> signInWithEmailLink(String link) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String counter = prefs.getString('emailLink');
    print(counter ?? '' + 'try to login');
    try {
      final result = await _firebaseAuth.signInWithEmailAndLink(
          email: counter, link: link);
      return result;
    } catch (e) {
      print(e.toString());
      // return false;
    }
    return null;
  }

  // Future<AuthResult> signInWithCredential(AuthCredential credential) async {
  //   return _firebaseAuth.signInWithCredential(credential);
  // }

  String _savedEmail = '';

  void initDynamicLinks() async {
    final auth = Provider.of<Auth>(context, listen: false);
    final PendingDynamicLinkData data =
        await FirebaseDynamicLinks.instance.getInitialLink();
    final Uri deepLink = data?.link;
    if (deepLink != null) {
      print('not null');
      // Navigator.pushNamed(context, deepLink.path);
    }
    print('login to $_savedEmail kill');
    if (deepLink != null) {
      try {
        auth.setLoadingValue(true);
        print('try login');
        final authResult =
            await signInWithEmailLink(deepLink.toString()).whenComplete(() {});
        print(authResult);
        print(authResult.user);
        print(authResult);
        if (authResult.user != null) {
          final auth = Provider.of<Auth>(context, listen: false);
          auth.setUser(authResult.user);

          print('navigate');
          Navigator.pushReplacementNamed(
            context,
            MyHomePage.route,
          );
        }
      } catch (e) {
        print(e);
      } finally {
        auth.setLoadingValue(false);
      }
    }

    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData dynamicLink) async {
      final Uri deepLink = dynamicLink?.link;

      if (deepLink != null) {
        print('not null');
        // Navigator.pushNamed(context, deepLink.path);
      }
    }, onError: (OnLinkErrorException e) async {
      print('onLinkError');
      print(e.message);
    });
  }

  registerOrLoginWithLink(auth) async {
    bool isHaveConnection = await CheckConnectivity.checkConnectivity();
    if (!isHaveConnection) {
      _showInformationSnackbar('No Internet');
      return;
    }
    bool emailCheck = _emailController.text.isEmpty;
    bool passwordCheck = _passwordController.text.isEmpty;
    bool condition =
        isForgotPassword ? emailCheck : emailCheck || passwordCheck;
    if (condition) {
      final snackBar = SnackBar(
        content: Text('Email' +
            (isForgotPassword ? ' ' : ' atau password ') +
            'tidak boleh kosong'),
        action: SnackBarAction(
          label: 'OK',
          onPressed: () {
            _scaffoldKey.currentState.hideCurrentSnackBar();
            // Some code to undo the change.
          },
        ),
      );

      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return;
    }

    try {
      if (!isForgotPassword) {
        final user = await auth.registerWithEmailAndPassword(
            _emailController.text, _passwordController.text);

        if (user != null) {
          await user.sendEmailVerification();
          _showInformationSnackbar(
              'success create user. email activation send to ${_emailController.text} before sign in');
        }
      } else {
        await sendSignInWithEmailLink(_emailController.text);

        _showInformationSnackbar('link sent to ${_emailController.text}');
      }
    } catch (e) {
      print('hello' + e.toString());
      _showInformationSnackbar(e.message.toString());
    }
  }

  loginEmailPasswordOrReset(auth) async {
    bool isHaveConnection = await CheckConnectivity.checkConnectivity();
    if (!isHaveConnection) {
      _showInformationSnackbar('No Internet');
      return;
    }
    bool emailCheck = _emailController.text.isEmpty;
    bool passwordCheck = _passwordController.text.isEmpty;
    bool condition =
        isForgotPassword ? emailCheck : emailCheck || passwordCheck;
    if (condition) {
      _showInformationSnackbar('Email' +
          (isForgotPassword ? ' ' : ' atau password ') +
          'tidak boleh kosong');
      return;
    }

    try {
      if (!isForgotPassword) {
        final user = await auth.signInWithEmailAndPassword(
            _emailController.text, _passwordController.text);

        if (user != null) {
          if (!user.isEmailVerified) {
            _showInformationSnackbar('activate your account');
          } else {
            _checkDataBeforeNavigate(user);
          }
        }
      } else {
        auth.resetPassword(_emailController.text);

        _showInformationSnackbar('check your email');
      }
    } catch (e) {
      _showInformationSnackbar(e.message);
      print(e);
    }
  }

  verifyOrSendOtp() async {
    bool isHaveConnection = await CheckConnectivity.checkConnectivity();
    if (!isHaveConnection) {
      _showInformationSnackbar('No Internet');
      return;
    }
    if (_phoneController.text.isEmpty) {
      _showInformationSnackbar('Nomor Telepon tidak boleh kosong');
      return;
    }

    if (isTypeOtp) {
      if (_otpController.text.isEmpty) {
        _showInformationSnackbar('OTP tidak boleh kosong');
        return;
      }
    }

    if (isTypeOtp) {
      senOtpManually(_otpController.text);
    } else {
      verifyPhoneNumber(context, '+93', _phoneController.text);
      countDownResendOtp = 15;
      _isShowResendOtpButton = true;
      setState(() {});
      _timer = Timer.periodic(Duration(seconds: 1), (_timer) {
        print(countDownResendOtp);
        countDownResendOtp--;
        setState(() {});
        if (countDownResendOtp <= 0) {
          _timer.cancel();
          _isShowResendOtpButton = false;
          setState(() {});
        }
      });
    }
    isTypeOtp = true;
    setState(() {});
  }

  resendOtp() async {
    bool isHaveConnection = await CheckConnectivity.checkConnectivity();
    if (!isHaveConnection) {
      _showInformationSnackbar('No Internet');
      return;
    }
    countDownResendOtp = 15;

    setState(() {});
    _timer = Timer.periodic(Duration(seconds: 1), (_timer) {
      print(countDownResendOtp);
      countDownResendOtp--;
      setState(() {});
      if (countDownResendOtp <= 0) {
        _timer.cancel();
        _isShowResendOtpButton = false;
        setState(() {});
      }
    });
    verifyPhoneNumber(context, '+93', _phoneController.text);
  }
  // @override
  // void didChangeDependencies() {
  //   super.didChangeDependencies();
  //   this.initDynamicLinks();
  // }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print(state);
    if (state == AppLifecycleState.resumed) {
      print('resume');
      initDynamicLinks();
      _printUser();
    }
  }

  _printUser() async {
    final user = await _firebaseAuth.currentUser();
    print(user?.uid);
  }

  // @override
  // didChangeDependencies() {
  //   super.didChangeDependencies();
  //   // final auth = Provider.of<Auth>(context);
  //   final value = Provider.of<Auth>(context).user;
  //   final isNavigate = Provider.of<Auth>(context).isInLoginPage;
  //   Provider.of<Auth>(context).setIsLoginValue(true);
  //   if (value != null) {
  //     print(value);
  //     print('is login page: ' + isNavigate.toString());
  //     if (isNavigate) {
  //       print('will navigate');
  //       Provider.of<Auth>(context).setIsLoginValue(false);
  //       _checkDataBeforeNavigate(value);
  //     }
  //   }
  // }
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final _emailFormKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<Auth>(context, listen: false);

    return Scaffold(
      key: _scaffoldKey,
      body: ModalProgressHUD(
        inAsyncCall: auth.isLoadingAuth,
        progressIndicator: SpinKitCircle(
          color: Colors.red,
        ),
        child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/rack.jpg"),
                  fit: BoxFit.cover,
                ),
              ),
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: _sigmaX, sigmaY: _sigmaY),
                child: Container(
                  color: Colors.black.withOpacity(_opacity),
                ),
              ),
            ),
            SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.1),
                      TypewriterAnimatedTextKit(
                          totalRepeatCount: 10,
                          speed: Duration(seconds: 1),
                          text: ['Login'],
                          textStyle: TextStyle(
                            fontSize: 30.0,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.start,
                          alignment: AlignmentDirectional
                              .topStart // or Alignment.topLeft

                          ),
                      SizedBox(height: 20),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 10,
                            height: 10,
                            decoration: BoxDecoration(
                                color: _selectedIndex == 0
                                    ? Colors.red
                                    : Colors.white,
                                borderRadius: BorderRadius.circular(10)),
                          ),
                          SizedBox(width: 10),
                          Container(
                            width: 10,
                            height: 10,
                            decoration: BoxDecoration(
                                color: _selectedIndex == 1
                                    ? Colors.red
                                    : Colors.white,
                                borderRadius: BorderRadius.circular(10)),
                          ),
                        ],
                      ),
                      SizedBox(height: 20),
                      SizedBox(
                        height: _selectedIndex == 0
                            ? 220 + (isForgotPassword ? 0.0 : 70.0)
                            : isTypeOtp ? 320 : 200,
                        child: PageView(
                          controller: _controller,
                          onPageChanged: _onItemTapped,
                          children: <Widget>[
                            Form(
                              key: _emailFormKey,
                              child: Card(
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: _selectedIndex == 0
                                      ? Column(
                                          children: <Widget>[
                                            TextFormField(
                                              keyboardType:
                                                  TextInputType.emailAddress,
                                              controller: _emailController,
                                              onChanged: (val) {
                                                _savedEmail = val;
                                              },
                                              decoration: InputDecoration(
                                                  labelText: 'Email',
                                                  hintText:
                                                      'your_email@provider.id'),
                                              validator: (value) {
                                                if (value.isEmpty) {
                                                  return 'Please enter some text';
                                                }
                                                return null;
                                              },
                                            ),
                                            SizedBox(height: 10),
                                            if (!isForgotPassword)
                                              TextFormField(
                                                obscureText: true,
                                                controller: _passwordController,
                                                decoration: InputDecoration(
                                                  labelText: 'Password',
                                                ),
                                                validator: (value) {
                                                  if (value.isEmpty) {
                                                    return 'Please enter some text';
                                                  }
                                                  return null;
                                                },
                                              ),
                                            SizedBox(height: 20),
                                            Row(
                                              children: <Widget>[
                                                Expanded(
                                                  child: Consumer<Auth>(
                                                    // child:
                                                    builder:
                                                        (BuildContext context,
                                                            auth,
                                                            Widget child) {
                                                      return RaisedButton(
                                                        color: Colors.white,
                                                        shape: RoundedRectangleBorder(
                                                            side: BorderSide(
                                                                width: 2,
                                                                color:
                                                                    Colors.red),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        25)),
                                                        child: Text(
                                                          isForgotPassword
                                                              ? 'Masuk dengan Link Email'
                                                              : 'Daftar',
                                                          textAlign:
                                                              TextAlign.center,
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.red),
                                                        ),
                                                        onPressed: () async {
                                                          registerOrLoginWithLink(
                                                              auth);
                                                        },
                                                      );
                                                    },
                                                  ),
                                                ),
                                                SizedBox(width: 20),
                                                Expanded(
                                                  child: Consumer<Auth>(
                                                    // child:
                                                    builder:
                                                        (BuildContext context,
                                                            auth,
                                                            Widget child) {
                                                      return RaisedButton(
                                                        color: Colors.red,
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        25)),
                                                        child: Text(
                                                          isForgotPassword
                                                              ? 'Reset Password'
                                                              : 'Masuk',
                                                          style: TextStyle(
                                                            color: Colors.white,
                                                          ),
                                                        ),
                                                        onPressed: () async {
                                                          loginEmailPasswordOrReset(
                                                              auth);
                                                        },
                                                      );
                                                    },
                                                  ),
                                                ),
                                              ],
                                            ),
                                            FlatButton(
                                              child: Text(isForgotPassword
                                                  ? 'Masuk/Daftar'
                                                  : 'Lupa Password?'),
                                              onPressed: () {
                                                isForgotPassword =
                                                    !isForgotPassword;
                                                setState(() {});
                                                print(auth.user?.uid);
                                              },
                                            )
                                          ],
                                        )
                                      : Container(),
                                ),
                              ),
                            ),
                            Card(
                              child: Padding(
                                padding: const EdgeInsets.all(15.0),
                                child: !(_selectedIndex == 1)
                                    ? Container()
                                    : Column(
                                        children: <Widget>[
                                          SizedBox(height: 10),
                                          isTypeOtp
                                              ? Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child:
                                                      Text('$userPhoneNumber'),
                                                )
                                              : Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      flex: 5,
                                                      child: TextField(
                                                        onEditingComplete:
                                                            () {},
                                                        focusNode:
                                                            _phoneFocusNode,
                                                        autocorrect: false,
                                                        keyboardType:
                                                            TextInputType
                                                                .number,
                                                        textInputAction:
                                                            TextInputAction
                                                                .next,
                                                        controller:
                                                            _phoneController,
                                                        decoration:
                                                            InputDecoration(
                                                          prefixText: '+93',
                                                          // border: OutlineInputBorder(),
                                                          labelText:
                                                              'Phone Number',
                                                          hintText:
                                                              '81234567890',
                                                        ),
                                                        onChanged: (val) {
                                                          userPhoneNumber =
                                                              '+62' + val;
                                                        },
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                          SizedBox(height: 10),
                                          isTypeOtp
                                              ? TextField(
                                                  onChanged: (val) {},
                                                  textInputAction:
                                                      TextInputAction.done,
                                                  focusNode: _passwordFocusNode,
                                                  controller: _otpController,
                                                  keyboardType:
                                                      TextInputType.number,
                                                  decoration: InputDecoration(
                                                    border:
                                                        OutlineInputBorder(),
                                                    labelText: 'Your OTP',
                                                    hintText: '123456',
                                                  ),
                                                  obscureText: true,
                                                )
                                              : Container(),
                                          SizedBox(height: 10),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            children: <Widget>[
                                              Expanded(
                                                child: Consumer<Auth>(
                                                  builder:
                                                      (BuildContext context,
                                                          Auth auth,
                                                          Widget child) {
                                                    return RaisedButton(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(20),
                                                      ),
                                                      color: Colors.green,
                                                      onPressed: () async {
                                                        verifyOrSendOtp();
                                                      },
                                                      child: Text(
                                                          isTypeOtp
                                                              ? 'Verify'
                                                              : 'Continue',
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .white)),
                                                    );
                                                  },
                                                ),
                                              ),
                                            ],
                                          ),
                                          SizedBox(height: 10),
                                          isTypeOtp
                                              ? Column(
                                                  children: <Widget>[
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: <Widget>[
                                                        if (countDownResendOtp >
                                                            0)
                                                          Text(
                                                              'Kirim ulang OTP dalam $countDownResendOtp detik'),
                                                        if (countDownResendOtp <=
                                                            0)
                                                          FlatButton(
                                                            child: Text(
                                                                'Kirim Ulang SMS'),
                                                            onPressed:
                                                                () async {
                                                              resendOtp();
                                                            },
                                                          ),
                                                        // Text('Tunggu x detik?'),
                                                      ],
                                                    ),
                                                    FlatButton(
                                                      child:
                                                          Text('Change number'),
                                                      onPressed: () {
                                                        isTypeOtp = false;
                                                        setState(() {});
                                                      },
                                                    )
                                                  ],
                                                )
                                              : Container(),
                                        ],
                                      ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 10),
                      SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: Divider(
                              color: Colors.white,
                              thickness: 3,
                            ),
                          ),
                          SizedBox(width: 10),
                          Text(
                            'or',
                            style: TextStyle(color: Colors.white),
                          ),
                          SizedBox(width: 10),
                          Expanded(
                            child: Divider(
                              color: Colors.white,
                              thickness: 3,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Sign in with ',
                            style: TextStyle(
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          FadeAnimatedTextKit(
                            totalRepeatCount: 30,
                            text: [
                              'google',
                              'facebook',
                              'twitter',
                              'social media',
                            ],
                            textStyle: TextStyle(
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Consumer<Auth>(builder: (context, auth, ch) {
                            return FloatingActionButton(
                                heroTag: null,
                                mini: true,
                                backgroundColor: Colors.grey[300],
                                child: Image.asset('assets/google-logo.png'),
                                onPressed: () async {
                                  bool isHaveConnection =
                                      await CheckConnectivity
                                          .checkConnectivity();
                                  if (!isHaveConnection) {
                                    _showInformationSnackbar('No Internet');
                                    return;
                                  }

                                  auth.setLoadingValue(true);

                                  final user = await auth.signInWithGoogle();
                                  if (user != null) {
                                    Navigator.pushNamed(
                                      context,
                                      MyHomePage.route,
                                    );
                                  }
                                  _checkDataBeforeNavigate(user);

                                  auth.setLoadingValue(false);
                                });
                          }),
                          Consumer<Auth>(builder: (context, auth, ch) {
                            return FloatingActionButton(
                              heroTag: null,
                              mini: true,
                              onPressed: () async {
                                bool isHaveConnection =
                                    await CheckConnectivity.checkConnectivity();
                                if (!isHaveConnection) {
                                  _showInformationSnackbar('No Internet');
                                  return;
                                }

                                auth.setLoadingValue(true);

                                final user = await auth.signInWithFacebook();
                                if (user != null) {
                                  Navigator.pushNamed(
                                    context,
                                    MyHomePage.route,
                                  );
                                }
                                auth.setLoadingValue(false);
                              },
                              backgroundColor: Colors.grey[300],
                              child: Image.asset(
                                'assets/facebook-logo.png',
                                // fit: BoxFit.scaleDown,
                              ),
                            );
                          }),
                          Consumer<Auth>(builder: (context, auth, ch) {
                            return FloatingActionButton(
                              heroTag: null,
                              mini: true,
                              onPressed: () async {
                                bool isHaveConnection =
                                    await CheckConnectivity.checkConnectivity();
                                if (!isHaveConnection) {
                                  _showInformationSnackbar('No Internet');
                                  return;
                                }

                                auth.setLoadingValue(true);
                                try {
                                  final user = await auth.signInWithTwitter();
                                  if (user != null) {
                                    Navigator.pushNamed(
                                      context,
                                      MyHomePage.route,
                                    );
                                  }
                                } catch (e) {
                                  print(e);
                                }

                                auth.setLoadingValue(false);
                              },
                              backgroundColor: Colors.blue,
                              child: Image.asset(
                                'assets/twitter-logo.png',
                                fit: BoxFit.cover,
                                // color: Colors.white,
                              ),
                            );
                          }),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
