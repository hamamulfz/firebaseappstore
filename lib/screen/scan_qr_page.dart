import 'dart:io';

import 'package:admob_flutter/admob_flutter.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:storewarehouse/models/database_product.dart';
import 'package:storewarehouse/services/auth.dart';
import 'package:storewarehouse/services/check_connectivity.dart';
import 'package:storewarehouse/services/firebase_database.dart';

import '../detail_screen.dart';
// import 'package:permission/permission.dart';
// import 'package:flutter_money_formatter/flutter_money_formatter.dart';

class ScanQrPage extends StatefulWidget {
  static String route = 'can_qr_page';
  ScanQrPage({
    this.isUpdate = false,
    this.product,
  });
  final bool isUpdate;
  final Product product;
  @override
  _ScanQrPageState createState() => _ScanQrPageState();
}

const String TEXT_SCANNER = 'TEXT_SCANNER';
const String BARCODE_SCANNER = 'BARCODE_SCANNER';

class _ScanQrPageState extends State<ScanQrPage> {
  // double _price = 0;

  // String _selectedScanner = TEXT_SCANNER;
  FirestoreDatabase _firestoreDatabase;

  String barcode = '';
  bool qrEnabled = false;
  bool isSendtoUserCloud = true;
  bool isSendToPublicCloud = false;

  TextEditingController codeQrResultController;
  TextEditingController nameController = TextEditingController();
  TextEditingController priceController = TextEditingController(text: '0');
  TextEditingController stockController = TextEditingController(text: '0');

  GlobalKey<ScaffoldState> scaffoldState = GlobalKey();

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  // static const String CAMERA_SOURCE = 'CAMERA_SOURCE';
  // static const String GALLERY_SOURCE = 'GALLERY_SOURCE';

  // final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  FocusNode scanResult = FocusNode();
  final _formKey = GlobalKey<FormState>();

  updateSelectedResult(String result, typeScanner) {
    if (TEXT_SCANNER == typeScanner) {
      nameController.text = result;
    } else {
      codeQrResultController.text = result;
    }
  }

  void onPickImageSelected(String _selectedScanner) async {
    var imageSource = ImageSource.camera;
    // final scaffold = _scaffoldKey.currentState;

    try {
      final file = await ImagePicker.pickImage(source: imageSource);
      if (file == null) {
        throw Exception('File is not available');
      }

      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              content:
                  DetailWidget(file, _selectedScanner, updateSelectedResult),
              actions: <Widget>[
                _selectedScanner == BARCODE_SCANNER
                    ? FlatButton(
                        child: Text('Use Other QR'),
                        onPressed: () {
                          Navigator.pop(context);
                          scan();
                        },
                      )
                    : Container(),
                FlatButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                )
              ],
            );
          });

      // Navigator.push(
      //   context,
      //   new MaterialPageRoute(
      //       builder: (context) => DetailWidget(file, _selectedScanner)),
      // );
    } catch (e) {
      // _scaffold.showSnackBar(SnackBar(
      //   content: Text(e.toString()),
      // ));
    }
  }

  FirebaseUser currentUser;

  _initializeTextBox() {
    codeQrResultController = widget.isUpdate
        ? TextEditingController(text: widget.product.code)
        : TextEditingController();

    nameController = widget.isUpdate
        ? TextEditingController(text: widget.product.name)
        : TextEditingController();
    priceController = widget.isUpdate
        ? TextEditingController(text: widget.product.price.toString())
        : TextEditingController();
    stockController = widget.isUpdate
        ? TextEditingController(text: widget.product.stock.toString())
        : TextEditingController();
  }

  @override
  void initState() {
    super.initState();
    if (!widget.isUpdate) scan();
    _initializeTextBox();
    Admob.initialize(getAppId());

    currentUser = Provider.of<Auth>(context, listen: false).user;

    _firestoreDatabase = FirestoreDatabase(uid: currentUser.uid);
  }

  @override
  Widget build(BuildContext context) {
    final bool isUpdate = widget.isUpdate;
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
          title: Text(isUpdate ? 'Edit Data' : 'Tambahkan Produk'),
        ),
        bottomNavigationBar: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: Builder(
                    builder: (BuildContext context) => RaisedButton(
                      color: Color.fromRGBO(58, 66, 86, 1.0),
                      shape: RoundedRectangleBorder(),
                      child: Text(
                        isUpdate ? 'Tambah' : 'Simpan',
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          // If the form is valid, display a Snackbar.
                          var qr = codeQrResultController.text;
                          var name = nameController.text;
                          var price = priceController.text;
                          var stok = stockController.text;

                          Product newProduct = new Product();
                          newProduct.code = codeQrResultController.text;
                          newProduct.name = nameController.text;
                          newProduct.price = int.parse(priceController.text);
                          newProduct.stock =
                              int.parse(stockController.text ?? '0');

                          final isWillAdd = await showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: Text('Tambahkan data'),
                                  content: Text(
                                      '$qr\n$name\n\nharga:\n $price\n\nStok:\n $stok'),
                                  actions: <Widget>[
                                    RaisedButton(
                                      color: Color.fromRGBO(58, 66, 86, 1.0),
                                      child: Text(
                                        'Ya',
                                        style: TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                      onPressed: () {
                                        Navigator.pop(context, true);
                                      },
                                    ),
                                    FlatButton(
                                      child: Text('Tidak'),
                                      onPressed: () {
                                        Navigator.pop(context, false);
                                      },
                                    ),
                                  ],
                                );
                              });

                          if (isWillAdd == true) {
                            // newProduct.createBy = currentUser;
                            await ProductProvider.insert(newProduct);
                            final isThereConnection =
                                await CheckConnectivity.checkConnectivity();
                            if (isThereConnection) {
                              if (isSendtoUserCloud)
                                _firestoreDatabase.createUserProductData(
                                    newProduct.toMap(), qr);
                              if (isSendToPublicCloud)
                                _firestoreDatabase.createPublicProductData(
                                    newProduct.toMap(), qr);
                            } else {
                              Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text(
                                    'No Internet. Will save to phone only'),
                              ));
                            }
                            Navigator.pop(context);
                          }
                        }

                        // Scaffold.of(context).showSnackBar(
                        //     SnackBar(content: Text('Lengkapi data')));
                      },
                    ),
                  ),
                ),
              ],
            ),
            AdmobBanner(
              adUnitId: getBannerAdUnitId(),
              adSize: AdmobBannerSize.BANNER,
              listener: (AdmobAdEvent event, Map<String, dynamic> args) {
                handleEvent(event, args, 'Banner');
              },
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 6,
                        child: TextField(
                            // inputFormatters: [
                            //   WhitelistingTextInputFormatter.digitsOnly
                            // ],
                            keyboardType: TextInputType.number,
                            textAlign: TextAlign.center,
                            focusNode: scanResult,
                            autocorrect: false,
                            autofocus: true,
                            enabled: qrEnabled,
                            controller: codeQrResultController,
                            onSubmitted: (val) {
                              qrEnabled = false;
                              setState(() {});
                            },
                            decoration: InputDecoration(
                              hintStyle: TextStyle(),
                              hintText: 'Write here',
                              border: qrEnabled ? null : InputBorder.none,
                            )),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: FlatButton(
                          padding: EdgeInsets.all(0),
                          child: Stack(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(top: 8, left: 8),
                                child: Icon(
                                  Icons.edit,
                                  color: Colors.black,
                                ),
                              ),
                              qrEnabled
                                  ? Center(
                                      child: Icon(
                                        Icons.do_not_disturb,
                                        size: 40,
                                        color: Colors.red,
                                      ),
                                    )
                                  : Container(),
                            ],
                          ),
                          onPressed: () {
                            qrEnabled = !qrEnabled;
                            FocusScope.of(context).requestFocus(scanResult);
                            setState(() {});
                          },
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: RaisedButton(
                            padding: EdgeInsets.all(0),
                            child: Icon(Icons.scanner),
                            onPressed: () {
                              // scan();
                              onPickImageSelected(BARCODE_SCANNER);
                            }),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 6,
                        child: TextFormField(
                          controller: nameController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: "Nama Produk",
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: RaisedButton(
                            padding: EdgeInsets.all(0),
                            child: Icon(Icons.scanner),
                            onPressed: () {
                              // scan();
                              onPickImageSelected(TEXT_SCANNER);
                            }),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    validator: (val) {
                      if (val.isEmpty) {
                        return 'Harus diisi';
                      } else if (!isNumeric(val)) {
                        return 'Harus angka';
                      }
                      return null;
                    },
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    keyboardType: TextInputType.number,
                    controller: priceController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: "Harga",
                    ),
                  ),
                  SizedBox(height: 15),
                  TextFormField(
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    keyboardType: TextInputType.number,
                    controller: stockController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: "Stok",
                    ),
                  ),
                  CheckboxListTile(
                    controlAffinity: ListTileControlAffinity.leading,
                    onChanged: (bool value) {
                      isSendtoUserCloud = value;
                      setState(() {});
                    },
                    value: isSendtoUserCloud,
                    title: Text('Sinkronkan ke akun saya'),
                  ),
                  CheckboxListTile(
                    controlAffinity: ListTileControlAffinity.leading,
                    onChanged: (bool value) {
                      isSendToPublicCloud = value;
                      setState(() {});
                    },
                    value: isSendToPublicCloud,
                    title: Text('Tambahkan ke data publik'),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future scan() async {
    try {
      String barcode = await BarcodeScanner.scan();
      codeQrResultController.text = barcode;
      qrEnabled = false;
      setState(() => this.barcode = barcode);
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        // var permissionNames =
        //     await Permission.requestPermissions([PermissionName.Camera]);
        // Permission.openSettings();
        setState(() {
          this.barcode = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
      }
    } on FormatException {
      setState(() => this.barcode =
          'null (User returned using the "back"-button before scanning anything. Result)');
    } catch (e) {
      setState(() => this.barcode = 'Unknown error: $e');
    }
  }

  void handleEvent(
      AdmobAdEvent event, Map<String, dynamic> args, String adType) {
    switch (event) {
      case AdmobAdEvent.loaded:
        showSnackBar('New Admob $adType Ad loaded!');
        break;
      case AdmobAdEvent.opened:
        showSnackBar('Admob $adType Ad opened!');
        break;
      case AdmobAdEvent.closed:
        showSnackBar('Admob $adType Ad closed!');
        break;
      case AdmobAdEvent.failedToLoad:
        showSnackBar('Admob $adType failed to load. :(');
        break;
      case AdmobAdEvent.rewarded:
        showDialog(
          context: scaffoldState.currentContext,
          builder: (BuildContext context) {
            return WillPopScope(
              child: AlertDialog(
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text('Reward callback fired. Thanks Andrew!'),
                    Text('Type: ${args['type']}'),
                    Text('Amount: ${args['amount']}'),
                  ],
                ),
              ),
              onWillPop: () async {
                scaffoldState.currentState.hideCurrentSnackBar();
                return true;
              },
            );
          },
        );
        break;
      default:
    }
  }

  void showSnackBar(String content) {
    scaffoldState.currentState.showSnackBar(SnackBar(
      content: Text(content),
      duration: Duration(milliseconds: 1500),
    ));
  }

  String getAppId() {
    if (Platform.isIOS) {
      return 'ca-app-pub-3940256099942544~1458002511';
    } else if (Platform.isAndroid) {
      return 'ca-app-pub-1987237703222831~6911440161';
    }
    return null;
  }

  String getBannerAdUnitId() {
    if (Platform.isIOS) {
      return 'ca-app-pub-3940256099942544/2934735716';
    } else if (Platform.isAndroid) {
      return 'ca-app-pub-1987237703222831/6636942141';
    }
    return null;
  }
}
