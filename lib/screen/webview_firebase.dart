import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class WebviewFirebase extends StatelessWidget {
  String url = "https://store-warehouse.web.app/#/";
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      url: url,
      initialChild: Container(
        child: const Center(
          child: CircularProgressIndicator(),
        ),
      ),
      withZoom: true,
      withLocalStorage: true,
      hidden: true,
      appBar: new AppBar(
        backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
        title: new ListTile(
          contentPadding: EdgeInsets.all(0),
          title: Text(
            "Web Store",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          subtitle: Text(
            url,
            style: TextStyle(color: Colors.white, fontSize: 15),
          ),
        ),
      ),
    );
  }
}
