import 'package:admob_flutter/admob_flutter.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:provider/provider.dart';
import 'package:storewarehouse/models/database_product.dart';
import 'package:storewarehouse/services/auth.dart';
import 'package:storewarehouse/services/check_connectivity.dart';
import 'package:storewarehouse/services/firebase_database.dart';

class UserCloudProduct extends StatefulWidget {
  final String uid;
  UserCloudProduct({
    this.uid,
  });

  @override
  _UserCloudProductState createState() => _UserCloudProductState();
}

class _UserCloudProductState extends State<UserCloudProduct> {
  bool _isLoading = false;
  ScrollController _scrollController;

  QuerySnapshot result;
  List<Product> _prodList = [];
  List<Product> filteredProductList = [];

  FirebaseUser currentUser;
  FirestoreDatabase _firestore;

  TextEditingController _searchProductController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  _showInformationSnackbar(String message) {
    final snackBar = SnackBar(
      content: Text(message),
      action: SnackBarAction(
        label: 'OK',
        onPressed: () {
          _scaffoldKey.currentState.hideCurrentSnackBar();
          // Some code to undo the change.
        },
      ),
    );

    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  getResult() async {
    bool isHaveConnection = await CheckConnectivity.checkConnectivity();
    if (!isHaveConnection) {
      _showInformationSnackbar('No Internet');
      return;
    }
    filteredProductList.clear();
    _prodList.clear();
    _isLoading = true;
    setState(() {});
    result = await _firestore.readUserProductData();
    result.documents.forEach((f) {
      Product newProd = new Product();
      newProd.code = f.data['code'];
      newProd.name = f.data['name'];
      newProd.stock = f.data['stock'];
      newProd.price = f.data['price'];
      _prodList.add(newProd);
    });
    print(_prodList.length);

    filteredProductList.addAll(_prodList);
    _isLoading = false;
    setState(() {});
  }

  filterProduct(String query) {
    _isLoading = true;
    setState(() {});
    if (query.isEmpty) {
      print('query is empty');
      filteredProductList.clear();
      filteredProductList.addAll(_prodList);
    } else {
      print('query is not empty');
      filteredProductList.clear();
      _prodList.forEach((data) {
        if (data.code.contains(query) ||
            data.name.toLowerCase().contains(query.toLowerCase())) {
          print('data found : ${data.name}');
          filteredProductList.add(data);
        }
      });
    }
    _isLoading = false;
    setState(() {});
  }

  String getAppId() {
    return 'ca-app-pub-1987237703222831~8653334033';
  }

  String getBannerAdUnitId() {
    return 'ca-app-pub-1987237703222831/7674831263';
  }

  @override
  void initState() {
    super.initState();
    currentUser = Provider.of<Auth>(context, listen: false).user;
    _firestore = FirestoreDatabase(uid: currentUser.uid);
    getResult();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
        // automaticallyImplyLeading: false,
        title: Text('Tersimpan di Cloud'),
        actions: <Widget>[
          IconButton(
            tooltip: 'Tambahkan Semua',
            icon: Icon(Icons.add_to_photos),
            onPressed: () async {
              final bool isAddAll = await showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text('Tambahkan Semua'),
                      content: Text(
                        'Apakan anda yakin ingin menambahkan semua?\nSebagian item akan tertimpa dengan data dari cloud.',
                      ),
                      actions: <Widget>[
                        RaisedButton(
                          color: Color.fromRGBO(58, 66, 86, 1.0),
                          child: Text('YA'),
                          onPressed: () {
                            Navigator.pop(context, true);
                          },
                        ),
                        FlatButton(
                          onPressed: () {
                            Navigator.pop(context, false);
                          },
                          child: Text('Tidak'),
                        )
                      ],
                    );
                  });
              if (isAddAll == null) return;
              if (isAddAll) {
                filteredProductList.forEach((single) async {
                  await ProductProvider.insert(single);
                });
                _scaffoldKey.currentState.showSnackBar(SnackBar(
                  duration: Duration(milliseconds: 600),
                  content: Text(
                      'added ${filteredProductList.length} product from cloud'),
                ));
              }
            },
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          // Text('Cari Produk'),
          // Divider(),
          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  height: 50,
                  width: 50,
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(width: 1, color: Colors.grey),
                  ),
                  child: TextField(
                    controller: _searchProductController,
                    onChanged: (va) => filterProduct(va),
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        tooltip: 'Cari barang dari daftar yang ada',
                        icon: Icon(
                          Icons.search,
                        ),
                        onPressed: () {},
                      ),
                      hintText: 'Nama Produk',
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.all(10),
                    ),
                  ),
                ),
              ),
              // IconButton(
              //   tooltip: 'Search by Barcode',
              //   icon: Icon(Icons.scanner),
              //   onPressed: () {
              //     // _scan();
              //   },
              // )
            ],
          ),
          if (!_isLoading)
            Center(
              child: Text(
                'Showing ${filteredProductList.length} Products',
                style: TextStyle(color: Colors.white),
              ),
            ),
          _isLoading
              ? Expanded(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                )
              : filteredProductList.length == 0
                  ? Center(
                      child: Text('No Data'),
                    )
                  : Expanded(
                      child: LiquidPullToRefresh(
                        onRefresh: () {
                          return getResult();
                        },
                        child: ListView.separated(
                          controller: _scrollController,
                          itemCount: filteredProductList.length,
                          physics: BouncingScrollPhysics(),
                          itemBuilder: (BuildContext context, int index) {
                            return Card(
                              elevation: 8,
                              margin: new EdgeInsets.symmetric(
                                  horizontal: 10.0, vertical: 6.0),
                              child: Container(
                                // margin: EdgeInsets.symmetric(horizontal: 10),

                                decoration: BoxDecoration(
                                  color: Color.fromRGBO(64, 75, 96, .9),
                                  // borderRadius: BorderRadius.circular(15),
                                  // border:
                                  //     Border.all(width: 2, color: Colors.grey)
                                ),
                                child: ListTile(
                                  // contentPadding: EdgeInsets.all(0),
                                  title: Text(
                                    filteredProductList[index].code,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 10,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  subtitle: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Text(
                                        filteredProductList[index].name,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 15,
                                        ),
                                      ),
                                      Text(
                                        'Stok ${filteredProductList[index].stock} - ${filteredProductList[index].price}',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 10,
                                        ),
                                      ),
                                    ],
                                  ),
                                  trailing: Container(
                                    decoration: BoxDecoration(
                                        border: new Border(
                                            left: new BorderSide(
                                                width: 1.0,
                                                color: Colors.white24))),
                                    child: Builder(
                                      builder: (BuildContext context) =>
                                          IconButton(
                                        tooltip: 'Tambahkan ke Produk Saya',
                                        icon: Icon(
                                          Icons.add_circle,
                                          color: Colors.white,
                                        ),
                                        onPressed: () async {
                                          Product product =
                                              filteredProductList[index];

                                          product.price = 0;
                                          product.stock = 0;

                                          await ProductProvider.insert(product);
                                          Scaffold.of(context)
                                              .showSnackBar(SnackBar(
                                            duration:
                                                Duration(milliseconds: 600),
                                            content: Text(
                                                'added to your products :\n${filteredProductList[index].name}'),
                                          ));
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                          separatorBuilder: (BuildContext context, int index) {
                            if ((index + 1) % 10 == 0 && index != 0) {
                              return Container(
                                margin: EdgeInsets.only(bottom: 20.0),
                                child: AdmobBanner(
                                  adUnitId: getBannerAdUnitId(),
                                  adSize: AdmobBannerSize.BANNER,
                                  listener: (AdmobAdEvent event,
                                      Map<String, dynamic> args) {
                                    // handleEvent(event, args, 'Banner');
                                  },
                                ),
                              );
                            }
                            return Container();
                          },
                        ),
                      ),
                    ),
        ],
      ),
    );
  }
}
