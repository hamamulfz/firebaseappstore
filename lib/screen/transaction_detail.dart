import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:storewarehouse/models/database_product.dart';
import 'package:storewarehouse/models/transaction_model.dart';
import 'package:storewarehouse/screen/add_transaction.dart';
import 'package:storewarehouse/services/auth.dart';
import 'package:storewarehouse/services/check_connectivity.dart';
import 'package:storewarehouse/services/firebase_database.dart';

class TransactionDetailScreen extends StatefulWidget {
  final Orders order;
  TransactionDetailScreen({
    this.order,
  });
  @override
  _TransactionDetailScreenState createState() =>
      _TransactionDetailScreenState();
}

class _TransactionDetailScreenState extends State<TransactionDetailScreen> {
  bool _isLoading = false;
  bool _isLoadingHUD = false;
  List<ProductOrder> filteredProductList = [];

  QuerySnapshot result;
  List<ProductOrder> _prodList = [];

  FirestoreDatabase _firestore;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextStyle style = TextStyle(
    color: Colors.white,
    fontSize: 15,
    fontWeight: FontWeight.bold,
  );

  _showInformationSnackbar(String message) {
    final snackBar = SnackBar(
      content: Text(message),
      action: SnackBarAction(
        label: 'OK',
        onPressed: () {
          _scaffoldKey.currentState.hideCurrentSnackBar();
          // Some code to undo the change.
        },
      ),
    );

    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  List<DocumentSnapshot> documentList;
  getDocumentList() async {
    var querySnapshot = await _firestore.readOrderDetails(
        widget.order.uid, widget.order.orderId);
    documentList = (querySnapshot).documents;
    return querySnapshot;
  }

  getResult() async {
    bool isHaveConnection = await CheckConnectivity.checkConnectivity();
    if (!isHaveConnection) {
      _showInformationSnackbar('No Internet');
      return;
    }
    filteredProductList.clear();
    _prodList.clear();
    _isLoading = true;
    setState(() {});
    result = await getDocumentList();
    result.documents.forEach((f) {
      ProductOrder newProd = new ProductOrder.fromJson(f.data);
      _prodList.add(newProd);
    });
    print(_prodList.length);

    filteredProductList.addAll(_prodList);
    _isLoading = false;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();

    _firestore = FirestoreDatabase(uid: widget.order.uid);
    getResult();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    FlutterMoneyFormatter fmf = new FlutterMoneyFormatter(
      amount: widget.order.total.toDouble(),
      settings: MoneyFormatterSettings(
        symbol: 'Rp',
        thousandSeparator: '.',
        decimalSeparator: ',',
        symbolAndNumberSeparator: ' ',
        fractionDigits: 0,
        // compactFormatType: CompactFormatType.sort
      ),
    );
    return ModalProgressHUD(
      inAsyncCall: _isLoadingHUD,
      child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
          appBar: AppBar(
            backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
            title: Text('Transaksi Detil'),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.share),
                onPressed: () async {
                  bool isShare = await showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          title: Text('Bagikan Invoice'),
                          content: Text('Bagikan invoice ini melalui web?'),
                          actions: <Widget>[
                            RaisedButton(
                              color: Color.fromRGBO(58, 66, 86, 1.0),
                              child: Text('YA'),
                              onPressed: () {
                                Navigator.pop(context, true);
                              },
                            ),
                            FlatButton(
                              onPressed: () {
                                Navigator.pop(context, false);
                              },
                              child: Text('Tidak'),
                            )
                          ],
                        );
                      });

                  if (isShare == null) return;
                  _isLoadingHUD = true;
                  setState(() {});
                  if (isShare) {
                    await FirestoreDatabase(uid: widget.order.uid)
                        .createPublicTxData(
                      widget.order.toJson(),
                      widget.order.orderId,
                    );

                    _prodList.forEach(
                      (single) async {
                        if (single.quantity != 0) {
                          await FirestoreDatabase(uid: widget.order.uid)
                              .createPublicTxDetailData(
                            single.toJson(),
                            widget.order.orderId,
                            single.code,
                          );
                        }
                      },
                    );
                  }
                  _isLoadingHUD = false;
                  setState(() {});
                },
              )
            ],
          ),
          body: Column(
            children: <Widget>[
              Card(
                child: ListTile(
                  title: Text(widget.order.costumerName),
                  subtitle: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('id: ' + widget.order.orderId),
                      Text("tanggal: " + widget.order.date),
                    ],
                  ),
                  trailing: Text(fmf.output.symbolOnLeft),
                ),
              ),
              if (!_isLoading && filteredProductList.length != 0)
                Center(
                  child: Text(
                    'Showing ${filteredProductList.length} Item',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              _isLoading
                  ? Expanded(
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    )
                  : filteredProductList.length == 0
                      ? Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                'No Data',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                ),
                              ),
                              RaisedButton(
                                child: Text('Tambah'),
                                onPressed: () async {
                                  bool newItem = await Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              AddTransaction()));
                                  if (newItem)
                                    _scaffoldKey.currentState
                                        .showSnackBar(SnackBar(
                                      content: Text('Berhasil Menyimpan'),
                                    ));
                                  // getResult();
                                },
                              )
                            ],
                          ),
                        )
                      : Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ListView.builder(
                              itemCount: filteredProductList.length,
                              itemBuilder: (BuildContext context, int i) {
                                FlutterMoneyFormatter fmfPrice =
                                    new FlutterMoneyFormatter(
                                  amount:
                                      filteredProductList[i].price.toDouble(),
                                  settings: MoneyFormatterSettings(
                                    symbol: 'Rp',
                                    thousandSeparator: '.',
                                    decimalSeparator: ',',
                                    symbolAndNumberSeparator: ' ',
                                    fractionDigits: 2,
                                    // compactFormatType: CompactFormatType.sort
                                  ),
                                );
                                FlutterMoneyFormatter fmfSubtotal =
                                    new FlutterMoneyFormatter(
                                  amount: filteredProductList[i]
                                      .subtotal
                                      .toDouble(),
                                  settings: MoneyFormatterSettings(
                                    symbol: 'Rp',
                                    thousandSeparator: '.',
                                    decimalSeparator: ',',
                                    symbolAndNumberSeparator: ' ',
                                    fractionDigits: 0,
                                    // compactFormatType: CompactFormatType.sort
                                  ),
                                );
                                return Card(
                                  elevation: 10,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Color.fromRGBO(64, 75, 96, .9),
                                      // borderRadius: BorderRadius.circular(15),
                                      // border:
                                      //     Border.all(width: 2, color: Colors.grey)
                                    ),
                                    child: ListTile(
                                      title: Text(
                                        filteredProductList[i].name,
                                        style: style,
                                      ),
                                      subtitle: Text(
                                        filteredProductList[i]
                                                .quantity
                                                .toString() +
                                            ' x ' +
                                            fmfPrice
                                                .output.withoutFractionDigits
                                                .toString(),
                                        style: style,
                                      ),
                                      trailing: Text(
                                        fmfSubtotal.output.symbolOnLeft
                                            .toString(),
                                        style: style,
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        ),
            ],
          )),
    );
  }
}
