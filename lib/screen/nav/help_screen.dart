import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
// import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:storewarehouse/services/auth.dart';
import 'package:storewarehouse/services/check_connectivity.dart';
import 'package:storewarehouse/services/firebase_database.dart';

class HelpScreen extends StatefulWidget {
  @override
  HelpScreenState createState() => HelpScreenState();
}

class HelpScreenState extends State<HelpScreen> {
  bool _isLoading = false;
  List filteredProductList = [];
  FirebaseUser currentUser;
  FirestoreDatabase _firestore;
  TextEditingController _requestController = TextEditingController();
  QuerySnapshot result;
  List<String> helpList = [];

  getResult() async {
    bool isHaveConnection = await CheckConnectivity.checkConnectivity();
    if (!isHaveConnection) {
      _showInformationSnackbar('No Internet');
      return;
    }
    filteredProductList.clear();
    _isLoading = true;
    setState(() {});
    helpList.clear();
    result = await _firestore.readHelp();
    result.documents.forEach((f) {
      helpList.add(f.data['name']);
    });
    print(helpList);
    filteredProductList.addAll(helpList);
    _isLoading = false;
    setState(() {});
  }

  searchProduct(String query) {
    _isLoading = true;
    setState(() {});
    filteredProductList.clear();
    print(filteredProductList.length);
    helpList.forEach((productlistarray) {
      if (productlistarray.toLowerCase().contains(query.toLowerCase())) {
        filteredProductList.add(productlistarray);
      }
    });

    _isLoading = false;
    setState(() {});
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  _showInformationSnackbar(String message) {
    final snackBar = SnackBar(
      duration: Duration(milliseconds: 600),
      content: Text(message),
      action: SnackBarAction(
        label: 'OK',
        onPressed: () {
          _scaffoldKey.currentState.hideCurrentSnackBar();
          // Some code to undo the change.
        },
      ),
    );

    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  void initState() {
    super.initState();
    currentUser = Provider.of<Auth>(context, listen: false).user;
    _firestore = FirestoreDatabase(uid: currentUser.uid);
    getResult();
  }

  bool isSearch = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      floatingActionButton: Padding(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.06),
        child: FloatingActionButton.extended(
          heroTag: null,
          tooltip: 'Minta Bantuan',
          icon: Icon(Icons.add),
          label: Text("Tambah"),
          onPressed: () async {
            bool isAdd = await showDialog(
                barrierDismissible: false,
                context: context,
                builder: (context) {
                  return AlertDialog(
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text('Nama produk yang dicari:'),
                        TextField(
                          controller: _requestController,
                          maxLines: 3,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: RaisedButton(
                                onPressed: () async {
                                  var sharedPreferences =
                                      await SharedPreferences.getInstance();
                                  SharedPreferences prefs = sharedPreferences;
                                  var uid = prefs.getString('user_uid');
                                  var request = _requestController.text;
                                  await _firestore.createHelp({
                                    "name": "$request",
                                    "create_by": '$uid'
                                  });
                                  Navigator.pop(context, true);
                                  _requestController.clear();
                                },
                                child: Text('Tambah'),
                              ),
                            ),
                            SizedBox(width: 15),
                            Expanded(
                              child: RaisedButton(
                                onPressed: () async {
                                  // _firestore.readUserProductData();
                                  Navigator.pop(context, false);
                                  _requestController.clear();
                                },
                                child: Text('Batal'),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  );
                });
            if (isAdd == null) return;
            if (isAdd) getResult();
          },
        ),
      ),
      backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
        automaticallyImplyLeading: false,
        title: Text('Minta Bantuan'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              isSearch = true;
              setState(() {});
            },
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          // Text('Help Request'),
          // Divider(),
          // Row(
          //   children: <Widget>[
          //     Expanded(
          //       child: RaisedButton(
          //         onPressed: () async {},
          //         child: Text('Buat Permintaan'),
          //       ),
          //     ),
          //   ],
          // ),
          if (isSearch)
            Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    height: 50,
                    width: 50,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(width: 1, color: Colors.grey),
                    ),
                    child: TextField(
                      // controller: _searchProductController,
                      onChanged: searchProduct,
                      decoration: InputDecoration(
                        suffixIcon: IconButton(
                          icon: Icon(
                            Icons.close,
                          ),
                          onPressed: () {
                            isSearch = false;
                            setState(() {});
                          },
                        ),
                        hintText: 'Nama Produk',
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.all(10),
                      ),
                    ),
                  ),
                ),
                // IconButton(
                //   tooltip: 'Search by Barcode',
                //   icon: Icon(Icons.scanner),
                //   onPressed: () {
                //     // _scan();
                //   },
                // )
              ],
            ),
          if (!isSearch)
            SizedBox(height: 10),

          if (!_isLoading)
            Text(
              'Terdapat ${helpList.length} Permintaan Bantuan',
              style: TextStyle(color: Colors.white),
            ),
          _isLoading
              ? Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                )
              // : filteredProductList.length == 0
              //     ? Center(
              //         child: Text('No Data'),
              //       )
              : Expanded(
                  child: RefreshIndicator(
                    onRefresh: () {
                      return getResult();
                    },
                    child: ListView.separated(
                      itemCount: filteredProductList.length,
                      physics: BouncingScrollPhysics(),
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () =>
                              _showInformationSnackbar('Fitur belum aktif'),
                          child: Card(
                            elevation: 8,
                            margin: new EdgeInsets.symmetric(
                                horizontal: 10.0, vertical: 6.0),
                            child: Container(
                              // margin: EdgeInsets.symmetric(horizontal: 10),

                              decoration: BoxDecoration(
                                color: Color.fromRGBO(64, 75, 96, .9),
                                // borderRadius: BorderRadius.circular(15),
                                // border:
                                //     Border.all(width: 2, color: Colors.grey)
                              ),
                              child: ListTile(
                                title: Text(
                                  '${filteredProductList[index]}',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                                trailing: IconButton(
                                  icon: Icon(
                                    Icons.center_focus_strong,
                                    color: Colors.white,
                                  ),
                                  onPressed: () {},
                                ),
                                // leading: Text(
                                //   'Rp \n 1000',
                                //   textAlign: TextAlign.center,
                                // ),
                              ),
                            ),
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        if ((index + 1) % 3 == 0 && index != 0) {
                          return Container(
                            margin: EdgeInsets.only(bottom: 20.0),
                            // child: AdmobBanner(
                            //   adUnitId: getBannerAdUnitId(),
                            //   adSize: AdmobBannerSize.BANNER,
                            //   listener: (AdmobAdEvent event,
                            //       Map<String, dynamic> args) {
                            //     handleEvent(event, args, 'Banner');
                            //   },
                            // ),
                          );
                        }
                        return Container();
                      },
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}
