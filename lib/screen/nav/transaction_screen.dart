import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:provider/provider.dart';
import 'package:storewarehouse/models/database_product.dart';
import 'package:storewarehouse/models/transaction_model.dart';
import 'package:storewarehouse/screen/add_transaction.dart';
import 'package:storewarehouse/screen/transaction_detail.dart';
import 'package:storewarehouse/services/auth.dart';
import 'package:storewarehouse/services/check_connectivity.dart';
import 'package:storewarehouse/services/firebase_database.dart';

class TransactionScreen extends StatefulWidget {
  @override
  _TransactionScreenState createState() => _TransactionScreenState();
}

class _TransactionScreenState extends State<TransactionScreen> {
  bool _isLoading = false;
  List<Orders> filteredProductList = [];
  ScrollController _scrollController;

  QuerySnapshot result;
  List<Orders> _prodList = [];

  FirebaseUser currentUser;
  FirestoreDatabase _firestore;

  TextEditingController _searchProductController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  TextStyle style = TextStyle(
    color: Colors.white,
    fontSize: 15,
    fontWeight: FontWeight.bold,
  );

  _showInformationSnackbar(String message) {
    final snackBar = SnackBar(
      content: Text(message),
      action: SnackBarAction(
        label: 'OK',
        onPressed: () {
          _scaffoldKey.currentState.hideCurrentSnackBar();
          // Some code to undo the change.
        },
      ),
    );

    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  List<DocumentSnapshot> documentList;
  getDocumentList(uid) async {
    var querySnapshot = await _firestore.readOrders(uid);
    documentList = (querySnapshot).documents;
    return querySnapshot;
  }

  getResult() async {
    bool isHaveConnection = await CheckConnectivity.checkConnectivity();
    if (!isHaveConnection) {
      _showInformationSnackbar('No Internet');
      return;
    }
    filteredProductList.clear();
    _prodList.clear();
    _isLoading = true;
    setState(() {});
    result = await getDocumentList(currentUser.uid);
    result.documents.forEach((f) {
      Orders newProd = new Orders.fromJson(f.data);
      _prodList.add(newProd);
    });
    print(_prodList.length);

    filteredProductList.addAll(_prodList);
    _isLoading = false;
    setState(() {});
  }

  filterProduct(String query) {
    _isLoading = true;
    setState(() {});
    if (query.isEmpty) {
      print('query is empty');
      filteredProductList.clear();
      filteredProductList.addAll(_prodList);
    } else {
      print('query is not empty');
      filteredProductList.clear();
      _prodList.forEach((data) {
        if (data.orderId.contains(query) ||
            data.costumerName.toLowerCase().contains(query.toLowerCase()) ||
            data.date.contains(query)) {
          // print('data found : ${data.name}');
          filteredProductList.add(data);
        }
      });
    }
    _isLoading = false;
    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    currentUser = Provider.of<Auth>(context, listen: false).user;
    _firestore = FirestoreDatabase(uid: currentUser.uid);
    getResult();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
        floatingActionButton: Padding(
          padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).viewInsets.bottom > 0
                  ? 0
                  : MediaQuery.of(context).size.height * 0.06),
          child: FloatingActionButton(
            heroTag: null,
            child: Icon(Icons.add),
            onPressed: () async {
              bool newItem = await Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => AddTransaction(),
              ));
              getResult();

              if (newItem == null) return;
              if (newItem)
                _scaffoldKey.currentState.showSnackBar(SnackBar(
                  content: Text('Berhasil Menyimpan'),
                ));
            },
          ),
        ),
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
          title: Text('Transaksi'),
        ),
        body: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    height: 50,
                    width: 50,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(width: 1, color: Colors.grey),
                    ),
                    child: TextField(
                      autofocus: false,
                      controller: _searchProductController,
                      onChanged: (va) => filterProduct(va),
                      decoration: InputDecoration(
                        suffixIcon: IconButton(
                          tooltip: 'Cari barang dari daftar yang ada',
                          icon: Icon(
                            Icons.search,
                          ),
                          onPressed: () {},
                        ),
                        hintText: 'Kode Transaksi/Nama/Tanggal',
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.all(10),
                      ),
                    ),
                  ),
                ),
                // IconButton(
                //   tooltip: 'Search by Barcode',
                //   icon: Icon(Icons.scanner),
                //   onPressed: () {
                //     // _scan();
                //   },
                // )
              ],
            ),
            if (!_isLoading && filteredProductList.length != 0)
              Center(
                child: Text(
                  'Showing ${filteredProductList.length} Products',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            _isLoading
                ? Expanded(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  )
                : filteredProductList.length == 0
                    ? Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'No Data',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                              ),
                            ),
                            RaisedButton(
                              child: Text('Tambah'),
                              onPressed: () async {
                                bool newItem = await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            AddTransaction()));
                                if (newItem)
                                  _scaffoldKey.currentState
                                      .showSnackBar(SnackBar(
                                    content: Text('Berhasil Menyimpan'),
                                  ));
                                getResult();
                              },
                            )
                          ],
                        ),
                      )
                    : Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ListView.builder(
                            itemCount: filteredProductList.length,
                            itemBuilder: (BuildContext context, int i) {
                              FlutterMoneyFormatter fmf =
                                  new FlutterMoneyFormatter(
                                amount: filteredProductList[i].total.toDouble(),
                                settings: MoneyFormatterSettings(
                                  symbol: 'Rp',
                                  thousandSeparator: '.',
                                  decimalSeparator: ',',
                                  symbolAndNumberSeparator: ' ',
                                  fractionDigits: 0,
                                  // compactFormatType: CompactFormatType.sort
                                ),
                              );
                              return Card(
                                elevation: 10,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Color.fromRGBO(64, 75, 96, .9),
                                    // borderRadius: BorderRadius.circular(15),
                                    // border:
                                    //     Border.all(width: 2, color: Colors.grey)
                                  ),
                                  child: ListTile(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  TransactionDetailScreen(
                                                    order:
                                                        filteredProductList[i],
                                                  )));
                                    },
                                    title: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          filteredProductList[i].orderId,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          filteredProductList[i].date,
                                          style: style,
                                        ),
                                      ],
                                    ),
                                    subtitle: Text(
                                      filteredProductList[i].costumerName,
                                      style: style,
                                    ),
                                    trailing: Text(
                                      fmf.output.symbolOnLeft,
                                      // filteredProductList[i].total.toString(),
                                      style: style,
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
          ],
        ));
  }
}
