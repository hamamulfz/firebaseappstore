import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:storewarehouse/models/database_product.dart';
import 'package:storewarehouse/screen/edit_profile.dart';
import 'package:storewarehouse/screen/login_screen.dart';
import 'package:storewarehouse/screen/user_cloud_product.dart';
import 'package:storewarehouse/screen/webview_firebase.dart';
import 'package:storewarehouse/services/auth.dart';
import 'package:storewarehouse/services/check_connectivity.dart';
import 'package:storewarehouse/services/firebase_database.dart';

class ProfileScreen extends StatefulWidget {
  ProfileScreen({this.logout});
  final Function(BuildContext context) logout;
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String name;
  FirebaseUser userAuth;
  String photoUrl;
  getUserData() async {
    userAuth = await FirebaseAuth.instance.currentUser();
    // print(userAuth?.displayName);
    // print(userAuth?.email);
    // print(userAuth?.phoneNumber);
    // print(userAuth?.photoUrl);
    // print(userAuth?.isEmailVerified);
    // print(userAuth?.providerId);
    // print(userAuth.providerId.contains('facebook.com'));
    // print(userAuth.providerId.contains('phone'));
    // print(userAuth?.uid);
    // print(auth?.uid);
    final token = await userAuth?.getIdToken();
    // var idToken = token.token;
    // print(idToken);
    photoUrl = userAuth.photoUrl;
    setState(() {});
    // print(_firebaseUser.);
  }

  Future<String> onRefresh() async {
    photoUrl = userAuth.photoUrl;
    setState(() {});
    return 'Okay';
  }

  bool isSubscribe = true;
  bool _isLoading = false;

  getSubscribe() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    isSubscribe = _prefs.getBool('isSubscribe') ?? true;
    setState(() {});
  }

  setSubscribe(bool value) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    await _prefs.setBool('isSubscribe', value);
    isSubscribe = value;
    setState(() {});
  }

  var style = TextStyle(
    color: Colors.white,
    fontSize: 15,
    // fontWeight: FontWeight.bold,
  );

  Future<String> getFcmToken() async {
    final token = await _firebaseMessaging.getToken();
    return token;
  }

  @override
  void initState() {
    super.initState();
    getUserData();
    getSubscribe();

    currentUser = Provider.of<Auth>(context, listen: false).user;
    _firestore = FirestoreDatabase(uid: currentUser.uid);
    getUserCloudData();
  }

  _showInformationSnackbar(String message) {
    final snackBar = SnackBar(
      duration: Duration(milliseconds: 600),
      content: Text(message),
      action: SnackBarAction(
        label: 'OK',
        onPressed: () {
          _scaffoldKey.currentState.hideCurrentSnackBar();
          // Some code to undo the change.
        },
      ),
    );

    // Find the Scaffold in the widget tree and use
    // it to show a SnackBar.
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  QuerySnapshot result;
  List<Product> _prodList = [];
  List<Product> filteredProductList = [];

  FirebaseUser currentUser;
  FirestoreDatabase _firestore;
  getUserCloudData() async {
    bool isHaveConnection = await CheckConnectivity.checkConnectivity();
    if (!isHaveConnection) {
      _showInformationSnackbar('No Internet');
      return;
    }
    filteredProductList.clear();
    _prodList.clear();
    result = await _firestore.readUserProductData();
    result.documents.forEach((f) {
      Product newProd = new Product();
      newProd.code = f.data['code'];
      newProd.name = f.data['name'];
      newProd.stock = f.data['stock'];
      newProd.price = f.data['price'];
      _prodList.add(newProd);
    });
    print(_prodList.length);

    filteredProductList.addAll(_prodList);
  }

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: _isLoading,
      child: Scaffold(
        key: _scaffoldKey,
        // resizeToAvoidBottomPadding: true,
        // resizeToAvoidBottomInset: true,
        backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
        body: LiquidPullToRefresh(
          onRefresh: onRefresh,
          showChildOpacityTransition: false,
          child: ListView(
            physics: BouncingScrollPhysics(),
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.1,
                  right: MediaQuery.of(context).size.width * 0.1,
                  top: MediaQuery.of(context).viewPadding.top +
                      MediaQuery.of(context).size.height * 0.02,
                ),

                height: MediaQuery.of(context).size.height * 0.3,
                width: MediaQuery.of(context).size.width * 0.5,
                // decoration: BoxDecoration(color: Colors.grey[400]),
                child: Card(
                  elevation: 10,
                  color: Colors.grey[400],
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 10),
                      Align(
                        alignment: Alignment.center,
                        child: SizedBox(
                          width: MediaQuery.of(context).size.height * 0.125,
                          height: MediaQuery.of(context).size.height * 0.125,
                          child: CircleAvatar(
                            radius: 80,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(100),
                              child: photoUrl == null
                                  ? Image.asset(
                                      'assets/avatar.jpg',
                                      fit: BoxFit.cover,
                                    )
                                  : Image.network(
                                      photoUrl,
                                      fit: BoxFit.cover,
                                    ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        userAuth?.displayName ?? 'Username',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 5),
                      Text(userAuth?.phoneNumber ?? userAuth?.email ?? 'email'),
                    ],
                  ),
                ),
              ),
              ListTile(
                onTap: () async {
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              EditProfileScreen(userAuth)));

                  photoUrl = userAuth.photoUrl;
                  setState(() {});
                },
                leading: Icon(
                  Icons.edit,
                  color: Colors.white,
                ),
                title: Text(
                  'Edit Profil',
                  style: style,
                ),
              ),
              ExpansionTile(
                title: ListTile(
                  contentPadding: EdgeInsets.all(0),
                  leading: Icon(
                    Icons.sync,
                    color: Colors.white,
                  ),
                  title: Text(
                    'Sinkronisasi',
                    style: style,
                  ),
                ),
                children: <Widget>[
                  ListTile(
                      title: Text(
                        'Cloud ke Lokal',
                        style: style,
                      ),
                      onTap: () async {
                        final bool isAddAll = await showDialog(
                            context: context,
                            builder: (context) {
                              return AlertDialog(
                                title: Text('Tambahkan Semua'),
                                content: Text(
                                  'Apakan anda yakin ingin menambahkan semua?\nSebagian item akan tertimpa dengan data dari cloud.',
                                ),
                                actions: <Widget>[
                                  RaisedButton(
                                    color: Color.fromRGBO(58, 66, 86, 1.0),
                                    child: Text('YA'),
                                    onPressed: () {
                                      Navigator.pop(context, true);
                                    },
                                  ),
                                  FlatButton(
                                    onPressed: () {
                                      Navigator.pop(context, false);
                                    },
                                    child: Text('Tidak'),
                                  )
                                ],
                              );
                            });
                        if (isAddAll == null) return;
                        if (isAddAll) {
                          filteredProductList.forEach((single) async {
                            await ProductProvider.insert(single);
                          });
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                            duration: Duration(milliseconds: 600),
                            content: Text(
                                'added ${filteredProductList.length} product from cloud'),
                          ));
                        }
                        // _showInformationSnackbar('Fitur belum aktif');
                      }),
                  ListTile(
                    title: Text(
                      'Lokal ke Cloud',
                      style: style,
                    ),
                    onTap: () => _showInformationSnackbar('Fitur belum aktif'),
                  ),
                  ListTile(
                      title: Text(
                        'Lihat Daftar di Cloud',
                        style: style,
                      ),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => UserCloudProduct(
                                  uid: userAuth.uid,
                                )));
                        // _showInformationSnackbar('Fitur belum aktif');
                      }),
                ],
              ),
              ExpansionTile(
                title: ListTile(
                  contentPadding: EdgeInsets.all(0),
                  leading: Icon(
                    Icons.notifications,
                    color: Colors.white,
                  ),
                  title: Text(
                    'Notifikasi',
                    style: style,
                  ),
                ),
                children: <Widget>[
                  Builder(
                    builder: (BuildContext context) => CheckboxListTile(
                      title: Text(
                        'Aktifkan Notifikasi Bantuan',
                        style: TextStyle(color: Colors.white),
                      ),
                      onChanged: (bool value) {
                        setSubscribe(value);
                        value
                            ? _firebaseMessaging.subscribeToTopic('helps')
                            : _firebaseMessaging.unsubscribeFromTopic('helps');
                        Scaffold.of(context).showSnackBar(SnackBar(
                          duration: Duration(seconds: 1),
                          content: Text(value
                              ? 'Notifikasi Bantuan Aktif'
                              : 'Notifikasi Bantuan Dimatikan'),
                        ));
                      },
                      value: isSubscribe,
                    ),
                  ),
                  ListTile(
                    title: Text(
                      'Uji Notifikasi',
                      style: style,
                    ),
                    onTap: () async {
                      FirestoreDatabase(uid: userAuth.uid)
                          .testNotifikasi({'token': await getFcmToken()});
                    },
                  )
                ],
              ),
              ListTile(
                leading: Icon(
                  Icons.update,
                  color: Colors.white,
                ),
                title: Text(
                  'Pembaruan Aplikasi',
                  style: style,
                ),
                onTap: () => _showInformationSnackbar('Fitur belum aktif'),
              ),
              ListTile(
                leading: Icon(
                  Icons.web,
                  color: Colors.white,
                ),
                title: Text(
                  'Visit our web',
                  style: style,
                ),
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => WebviewFirebase()));
                },
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.9,
                child: Divider(
                  color: Colors.white,
                ),
              ),
              Consumer<Auth>(
                builder: (context, auth, ch) => ListTile(
                  leading: Icon(
                    Icons.exit_to_app,
                    color: Colors.red,
                  ),
                  title: Text(
                    'Keluar',
                    style: TextStyle(color: Colors.red),
                  ),
                  onTap: () async {
                    _isLoading = true;
                    setState(() {});
                    await auth.signOut();
                    _firebaseMessaging.unsubscribeFromTopic('helps');
                    _isLoading = false;
                    setState(() {});

                    await Navigator.of(context).pushNamedAndRemoveUntil(
                        LoginScreen.route, (Route<dynamic> route) => false);
                    final authbool = auth.isInLoginPage;
                    auth.setLoadingValue(false);

                    if (!authbool) auth.setIsLoginValue(true);
                    // widget.logout(context);
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
