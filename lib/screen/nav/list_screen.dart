import 'package:admob_flutter/admob_flutter.dart';
import 'package:barcode_scan/barcode_scan.dart';
// import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
// import 'package:storewarehouse/data.dart';
import 'package:storewarehouse/models/database_product.dart';
import 'package:storewarehouse/screen/scan_qr_page.dart';
// import 'package:storewarehouse/services/firebase_database.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';

class ListScreen extends StatefulWidget {
  static String route = 'home_screen';

  ListScreen({
    Key key,
    this.title = 'Produk Toko',
  }) : super(key: key);

  final String title;

  @override
  _ListScreenState createState() => _ListScreenState();
}

class _ListScreenState extends State<ListScreen> with WidgetsBindingObserver {
  List<Product> filteredProductList = [];
  List<Product> productList;
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey();
  bool _isLoading = false;
  TextEditingController searchProductController = TextEditingController();
  String barcode = '';

  getAllProduct() async {
    _isLoading = true;
    print('loading...');
    setState(() {});
    filteredProductList.clear();
    productList = await ProductProvider.getFastingByDone(false);
    print(productList.length);
    productList.forEach((prod) {
      filteredProductList.add(prod);
    });
    // widget.updateCount(fasting.length);
    print('end loading...');
    _isLoading = false;
    setState(() {});
  }

  deleteAllProduct() async {
    await ProductProvider().deleteAll();
    getAllProduct();
  }

  searchProduct(String query) {
    _isLoading = true;
    setState(() {});
    print(productList.length);
    print(filteredProductList.length);
    print('okay');
    filteredProductList.clear();
    print(productList.length);
    print(filteredProductList.length);
    productList.forEach((productlistarray) {
      // print(productlistarray.name);
      if (productlistarray.code.toString().contains(query) ||
          productlistarray.name.toLowerCase().contains(query.toLowerCase())) {
        // print('ada');
        filteredProductList.add(productlistarray);
      }

      // if (productlistarray.name.toLowerCase().contains(query.toLowerCase())) {
      //   print('nomor');
      // }
    });

    print(productList.length);
    print(filteredProductList.length);
    _isLoading = false;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    getAllProduct();
    Admob.initialize(getAppId());
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      // user returned to our app
      print('PRINTresumed');

      getAllProduct();
    } else if (state == AppLifecycleState.inactive) {
      print('PRINT inactive');
      // app is inactive
    } else if (state == AppLifecycleState.paused) {
      print('PRINT paused');
      // user is about quit our app temporally
    } else if (state == AppLifecycleState.detached) {
      // app suspended (not used in iOS)
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
        automaticallyImplyLeading: false,
        title: Text(widget.title),
        actions: <Widget>[
          // IconButton(
          //   icon: Icon(Icons.sync),
          //   onPressed: () {
          //     // getAllProduct();
          //   },
          // ),
          IconButton(
            tooltip: 'Refresh',
            icon: Icon(Icons.refresh),
            onPressed: () {
              getAllProduct();
            },
          ),
          IconButton(
            tooltip: 'Hapus Semua',
            icon: Icon(Icons.delete),
            onPressed: () async {
              final bool isAddAll = await showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text('Hapus Semua'),
                      content: Text(
                        'Apakan anda yakin ingin menghapus semua?',
                      ),
                      actions: <Widget>[
                        FlatButton(
                          child: Text('YA'),
                          onPressed: () {
                            Navigator.pop(context, true);
                          },
                        ),
                        RaisedButton(
                          color: Color.fromRGBO(58, 66, 86, 1.0),
                          onPressed: () {
                            Navigator.pop(context, false);
                          },
                          child: Text('Tidak'),
                        )
                      ],
                    );
                  });
              if (isAddAll == null) return;
              if (isAddAll) deleteAllProduct();
            },
          ),
        ],
      ),

      // bottomNavigationBar:
      // AdmobBanner(
      //   adUnitId: getBannerAdUnitId(),
      //   adSize: AdmobBannerSize.BANNER,
      //   listener: (AdmobAdEvent event, Map<String, dynamic> args) {
      //     handleEvent(event, args, 'Banner');
      //   },
      // ),
      body: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  height: 50,
                  width: 50,
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(255, 255, 255, 0.8),
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(width: 1, color: Colors.grey),
                  ),
                  child: TextField(
                    controller: searchProductController,
                    onChanged: (val) {
                      searchProduct(val);
                      if (val.isEmpty) {
                        getAllProduct();
                      }
                    },
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: Icon(
                          Icons.search,
                        ),
                        onPressed: () {
                          searchProduct(barcode);
                          setState(() {});
                        },
                      ),
                      hintText: 'Nama Produk',
                      border: InputBorder.none,
                      // contentPadding: EdgeInsets.all(10),
                    ),
                  ),
                ),
              ),
              IconButton(
                tooltip: 'Search by Barcode',
                icon: Icon(
                  Icons.scanner,
                  color: Colors.white,
                ),
                onPressed: () {
                  _scan();
                },
              )
            ],
          ),
          filteredProductList == null || _isLoading
              ? Expanded(
                  child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: Center(child: CircularProgressIndicator())))
              : filteredProductList.length == 0
                  ? Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'No Data',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            ),
                          ),
                          RaisedButton(
                            child: Text('Tambah'),
                            onPressed: () async {
                              await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          ScanQrPage()));
                              getAllProduct();
                            },
                          )
                        ],
                      ),
                    )
                  : Expanded(
                      child: LiquidPullToRefresh(
                        showChildOpacityTransition: false,
                        onRefresh: () {
                          return getAllProduct();
                        },
                        child: ListView.separated(
                          physics: BouncingScrollPhysics(),
                          itemBuilder: (BuildContext context, int index) {
                            FlutterMoneyFormatter fmf =
                                new FlutterMoneyFormatter(
                              amount:
                                  filteredProductList[index].price.toDouble(),
                              settings: MoneyFormatterSettings(
                                symbol: 'Rp',
                                thousandSeparator: '.',
                                decimalSeparator: ',',
                                symbolAndNumberSeparator: ' ',
                                fractionDigits: 2,
                                // compactFormatType: CompactFormatType.sort
                              ),
                            );
                            return Dismissible(
                              confirmDismiss: (dir) async {
                                if (dir == DismissDirection.startToEnd) {
                                  return false;
                                }
                                if (dir == DismissDirection.endToStart) {
                                  bool confirm = await showDialog(
                                      context: context,
                                      builder: (context) {
                                        return AlertDialog(
                                          title: Text('Hapus Data'),
                                          content: Text(
                                              'Apakah anda yakin ingin menghapus data ini?\n\n ${filteredProductList[index].name}'),
                                          actions: <Widget>[
                                            RaisedButton(
                                              color: Colors.red,
                                              onPressed: () {
                                                Navigator.pop(context, false);
                                              },
                                              child: Text('Tidak'),
                                            ),
                                            FlatButton(
                                              onPressed: () {
                                                Navigator.pop(context, true);
                                              },
                                              child: Text('Ya'),
                                            )
                                          ],
                                        );
                                      });
                                  return confirm;
                                }
                                return null;
                              },
                              onDismissed: (dir) async {
                                if (dir == DismissDirection.startToEnd) {
                                  await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          ScanQrPage(
                                        isUpdate: true,
                                        product: filteredProductList[index],
                                      ),
                                    ),
                                  );
                                  getAllProduct();
                                } else if (dir == DismissDirection.endToStart) {
                                  ProductProvider()
                                      .delete(filteredProductList[index].id);
                                  filteredProductList
                                      .remove(filteredProductList[index]);
                                  setState(() {});
                                }
                                // print(filteredProductList.length);
                                // if (filteredProductList.length == 0) {
                                //   setState(() {});
                                // }
                              },
                              child: Card(
                                elevation: 8,
                                margin: new EdgeInsets.symmetric(
                                    horizontal: 10.0, vertical: 6.0),
                                child: Container(
                                  // margin: EdgeInsets.symmetric(horizontal: 10),

                                  decoration: BoxDecoration(
                                    color: Color.fromRGBO(64, 75, 96, .9),
                                    // borderRadius: BorderRadius.circular(15),
                                    // border:
                                    //     Border.all(width: 2, color: Colors.grey)
                                  ),
                                  child: ListTile(
                                    onTap: () async {
                                      await Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              ScanQrPage(
                                            isUpdate: true,
                                            product: filteredProductList[index],
                                          ),
                                        ),
                                      );
                                      getAllProduct();
                                    },
                                    isThreeLine: false,
                                    // contentPadding: EdgeInsets.symmetric(
                                    //     horizontal: 20.0, vertical: 20.0),
                                    trailing: Container(
                                        padding: EdgeInsets.only(
                                          right: 12.0,
                                          top: 5,
                                        ),
                                        decoration: new BoxDecoration(
                                            border: new Border(
                                                right: new BorderSide(
                                                    width: 2.0,
                                                    color: Colors.white24))),
                                        child: RichText(
                                          textAlign: TextAlign.end,
                                          text: TextSpan(children: [
                                            TextSpan(
                                              text:
                                                  '${filteredProductList[index].stock}',
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            TextSpan(text: '\nTersisa')
                                          ]),
                                        )
                                        // Text(
                                        //   '${filteredProductList[index].stock}\nTersisa',
                                        //   style: TextStyle(
                                        //     color: Colors.white,
                                        //     fontSize: 20,
                                        //     fontWeight: FontWeight.bold,
                                        //   ),
                                        // ),
                                        ),
                                    title: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          filteredProductList[index].code,
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12,
                                          ),
                                        ),
                                        Text(
                                          filteredProductList[index].name,
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ],
                                    ),
                                    // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

                                    subtitle: Text(fmf.output.symbolOnLeft,
                                        style: TextStyle(color: Colors.white)),

                                    // leading: Icon(Icons.keyboard_arrow_right,
                                    //     color: Colors.white, size: 30.0),
                                  ),
                                ),
                              ),
                              // ListTile(
                              //   title: Text(filteredProductList[index].name),
                              //   subtitle: Text(filteredProductList[index].code),
                              //   trailing: Text(fmf.output.symbolOnLeft),
                              //   leading: Text(
                              //     'Stok \n ${filteredProductList[index].stock.toString()}',
                              //     textAlign: TextAlign.center,
                              //   ),
                              // ),
                              key: UniqueKey(),
                              background: Container(
                                color: Colors.green,
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 18.0),
                                    child: Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.edit,
                                          color: Colors.white,
                                        ),
                                        SizedBox(width: 10),
                                        Text(
                                          'Tap untuk edit',
                                          style: TextStyle(color: Colors.white),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              secondaryBackground: Container(
                                color: Colors.redAccent,
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 18.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        Text(
                                          'Hapus',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        SizedBox(width: 10),
                                        Icon(
                                          Icons.delete,
                                          color: Colors.white,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                          itemCount: filteredProductList.length,
                          separatorBuilder: (BuildContext context, int index) {
                            return Container();
                          },
                        ),
                      ),
                    ),
        ],
      ),

      floatingActionButton: Padding(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.06),
        child: FloatingActionButton(
          heroTag: null,
          onPressed: () async {
            // final data = dataUploadToCloud;
            // final user = await FirebaseAuth.instance.currentUser();
            // final firestore = FirestoreDatabase(uid: user.uid);
            // try {
            //   data.forEach((product) {
            //     firestore.createPublicProductData(product, product['code']);
            //   });
            // } catch (e) {
            //   print(e);
            // }
            await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => ScanQrPage()));
            getAllProduct();
          },
          tooltip: 'Tambah Produk',
          child: Icon(Icons.add),
        ),
      ),
    );
  }

  Future _scan() async {
    try {
      String barcode = await BarcodeScanner.scan();
      // codeQrResultController.text = barcode;
      // qrEnabled = false;
      searchProduct(barcode);
      searchProductController.text = barcode;
      this.barcode = barcode;
      setState(() {});
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        // var permissionNames =
        //     await Permission.requestPermissions([PermissionName.Camera]);
        // Permission.openSettings();
        setState(() {
          this.barcode = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
      }
    } on FormatException {
      setState(() => this.barcode =
          'null (User returned using the "back"-button before scanning anything. Result)');
    } catch (e) {
      setState(() => this.barcode = 'Unknown error: $e');
    }
  }

  void handleEvent(
      AdmobAdEvent event, Map<String, dynamic> args, String adType) {
    switch (event) {
      case AdmobAdEvent.loaded:
        showSnackBar('New Admob $adType Ad loaded!');
        break;
      case AdmobAdEvent.opened:
        showSnackBar('Admob $adType Ad opened!');
        break;
      case AdmobAdEvent.closed:
        showSnackBar('Admob $adType Ad closed!');
        break;
      case AdmobAdEvent.failedToLoad:
        showSnackBar('Admob $adType failed to load. :(');
        break;
      case AdmobAdEvent.rewarded:
        showDialog(
          context: scaffoldState.currentContext,
          builder: (BuildContext context) {
            return WillPopScope(
              child: AlertDialog(
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text('Reward callback fired. Thanks Andrew!'),
                    Text('Type: ${args['type']}'),
                    Text('Amount: ${args['amount']}'),
                  ],
                ),
              ),
              onWillPop: () async {
                scaffoldState.currentState.hideCurrentSnackBar();
                return true;
              },
            );
          },
        );
        break;
      default:
    }
  }

  void showSnackBar(String content) {
    scaffoldState.currentState.showSnackBar(SnackBar(
      content: Text(content),
      duration: Duration(milliseconds: 1500),
    ));
  }

  String getAppId() {
    return 'ca-app-pub-1987237703222831~8653334033';
  }

  String getBannerAdUnitId() {
    return 'ca-app-pub-1987237703222831/7674831263';
  }
}
