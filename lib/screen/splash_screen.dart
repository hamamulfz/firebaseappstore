import 'dart:async';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:storewarehouse/screen/login_screen.dart';
import 'package:storewarehouse/screen/my_home_page.dart';
import 'package:storewarehouse/services/auth.dart';

class SplashScreen extends StatefulWidget {
  static String route = 'splash_screen';

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  // static FirebaseInAppMessaging fiam = FirebaseInAppMessaging();

  routeNavigation() async {
    final authUser = await _firebaseAuth.currentUser();
    final user = authUser;

    print(user);
    user == null
        ? Navigator.pushReplacementNamed(context, LoginScreen.route)
        : Navigator.pushReplacementNamed(context, MyHomePage.route);
  }

  Timer _timer;
  bool isLoading = false;
  bool isShowNotif = false;

  // String _homeScreenText = "Waiting for token...";

  @override
  void initState() {
    super.initState();
    Provider.of<Auth>(context, listen: false).checkUser();

    _timer = Timer(Duration(seconds: 2), () {
      isLoading = true;
      setState(() {});
      routeNavigation();
      _timer.cancel();
    });
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red,
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(colors: <Color>[
            Colors.grey[200],
            Colors.grey[100],
          ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'assets/shop.webp',
              width: 250,
              height: 250,
              // color: Colors.white,
            ),
            SizedBox(height: 15),
            Center(
              child: TyperAnimatedTextKit(
                  text: ['Store\nWarehouse'],
                  textStyle: TextStyle(
                    fontSize: 20.0,
                  ),
                  textAlign: TextAlign.center,
                  alignment: AlignmentDirectional.topStart),
            ),
            // isLoading
            //     ? Center(child: CircularProgressIndicator())
            //     : Container(),
          ],
        ),
      ),
    );
  }
}

class Item {
  Item({this.itemId});
  final String itemId;

  StreamController<Item> _controller = StreamController<Item>.broadcast();
  Stream<Item> get onChanged => _controller.stream;

  String _status;
  String get status => _status;
  set status(String value) {
    _status = value;
    _controller.add(this);
  }

  static final Map<String, Route<void>> routes = <String, Route<void>>{};
}
