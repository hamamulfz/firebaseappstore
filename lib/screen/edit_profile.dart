import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class EditProfileScreen extends StatefulWidget {
  static String route = 'edit_profile';
  final FirebaseUser user;
  EditProfileScreen(this.user);
  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  TextEditingController _userNameController = TextEditingController();
  TextEditingController _userEmailController = TextEditingController();
  TextEditingController _userPhoneController = TextEditingController();
  String photoUrl;
  StreamSubscription<StorageTaskEvent> streamSubscription;
  bool isLoading = false;

  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  setPhotoUrl(String url) {
    UserUpdateInfo userInfo = UserUpdateInfo();
    userInfo.photoUrl = url;
    try {
      widget.user.updateProfile(userInfo);
    } catch (e) {
      print(e);
    }
  }

  setUserDetail() {
    UserUpdateInfo userInfo = UserUpdateInfo();
    userInfo.displayName = _userNameController.text;
    print(_userNameController.text);
    try {
      widget.user.updateProfile(userInfo);
      // widget.user.updateEmail(email);
    } catch (e) {
      print(e);
    }
  }

  Future<String> _pickSaveImage(source) async {
    isLoading = true;
    setState(() {});
    File imageFile = await ImagePicker.pickImage(source: source);
    if (imageFile == null) return null;
    File cropped = await _cropImage(imageFile);
    if (cropped == null) return null;
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text('Uploading...'),
      duration: Duration(milliseconds: 800),
    ));

    StorageReference ref = FirebaseStorage.instance
        .ref()
        .child('users/' + widget.user.uid)
        .child("profile.jpg");
    StorageUploadTask uploadTask = ref.putFile(cropped);
    // streamSubscription = uploadTask.events.listen((event) {
    //   // You can use this to notify yourself or your user in any kind of way.
    //   // For example: you could use the uploadTask.events stream in a StreamBuilder instead
    //   // to show your user what the current status is. In that case, you would not need to cancel any
    //   // subscription as StreamBuilder handles this automatically.

    //   // Here, every StorageTaskEvent concerning the upload is printed to the logs.
    //   print('EVENT ${event.type}');
    // });

    return await (await uploadTask.onComplete).ref.getDownloadURL();
  }

  Future<File> _cropImage(_imageFile) async {
    File cropped = await ImageCropper.cropImage(
        sourcePath: _imageFile.path,
        // maxWidth: 512,
        // maxHeight: 512,
        aspectRatio: CropAspectRatio(
          ratioX: 1.0,
          ratioY: 1.0,
        ));

    setState(() {
      _imageFile = cropped ?? _imageFile;
    });
    return cropped;
  }

  @override
  void initState() {
    super.initState();
    photoUrl = widget.user.photoUrl;
    print(widget.user.displayName);
    if (widget.user.displayName != null)
      _userNameController =
          TextEditingController(text: widget.user.displayName);
  }

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: isLoading,
      child: Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
          title: Text('Update Profil'),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(
              top: MediaQuery.of(context).viewPadding.top,
              bottom: MediaQuery.of(context).viewPadding.bottom,
              left: 10,
              right: 10,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Center(
                  child: GestureDetector(
                    onTap: () async {
                      var source = await scaffoldKey.currentState
                          .showBottomSheet((context) {
                        return Card(
                          elevation: 10,
                          color: Colors.grey[200],
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.3,
                                child: Divider(),
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.8,
                                child: Divider(),
                              ),
                              Card(
                                elevation: 10,
                                child: ListTile(
                                  title: Text('From Camera'),
                                  onTap: () async {
                                    photoUrl = await _pickSaveImage(
                                        ImageSource.camera);
                                    // streamSubscription.cancel();
                                    print(photoUrl);
                                    if (photoUrl == null) return;
                                    await setPhotoUrl(photoUrl);

                                    isLoading = false;
                                    setState(() {});
                                    Navigator.pop(context);
                                  },
                                ),
                              ),
                              Card(
                                elevation: 10,
                                child: ListTile(
                                  title: Text('From Gallery'),
                                  onTap: () async {
                                    photoUrl = await _pickSaveImage(
                                        ImageSource.gallery);

                                    // streamSubscription.cancel();
                                    print(photoUrl);
                                    if (photoUrl == null) return;
                                    await setPhotoUrl(photoUrl);

                                    isLoading = false;
                                    setState(() {});
                                    Navigator.pop(context);
                                  },
                                ),
                              ),
                            ],
                          ),
                        );
                      });
                      print(source);
                      // photoUrl = await _pickSaveImage(widget.user.uid);
                    },
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: Material(
                        elevation: 20,
                        child: Stack(
                          children: <Widget>[
                            CircleAvatar(
                              radius: 80,
                              child: photoUrl == null
                                  ? Image.asset(
                                      'assets/avatar.jpg',
                                      fit: BoxFit.cover,
                                    )
                                  : Image.network(
                                      photoUrl,
                                      fit: BoxFit.cover,
                                    ),
                            ),
                            Positioned(
                              bottom: 0,
                              right: 0,
                              left: 0,
                              child: Container(
                                color: Color(0x75000000),
                                padding: EdgeInsets.only(bottom: 20),
                                child: Text(
                                  'Change Photo',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 15),
                Text('Nama'),
                TextField(
                  controller: _userNameController,
                ),
                // SizedBox(height: 10),
                // Text('Email'),
                // TextField(
                //   controller: _userEmailController,
                // ),
                // SizedBox(height: 10),
                // Text(
                //   'Nomor Telepon',
                // ),
                // TextField(
                //   controller: _userPhoneController,
                // ),
                SizedBox(height: 10),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Builder(
                        builder: (BuildContext context) => RaisedButton(
                          color: Color.fromRGBO(58, 66, 86, 1.0),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          child: Text(
                            'Simpan',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                          onPressed: () {
                            if (_userNameController.text.isEmpty) {
                              Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text('Isi kolom nama '),
                                action: SnackBarAction(
                                  label: 'OK',
                                  onPressed: () {
                                    // Navigator.of(context).pop();
                                  },
                                ),
                              ));
                            }

                            isLoading = true;
                            setState(() {});
                            setUserDetail();

                            isLoading = false;
                            setState(() {});
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text('Berhasil menyimpan nama. '),
                              action: SnackBarAction(
                                label: 'OK',
                                onPressed: () {
                                  // Navigator.of(context).pop();
                                },
                              ),
                            ));
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
