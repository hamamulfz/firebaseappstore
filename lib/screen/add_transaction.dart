import 'package:flutter/material.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:storewarehouse/models/database_product.dart';
import 'package:storewarehouse/models/transaction_model.dart';
import 'package:intl/intl.dart';
import 'package:storewarehouse/services/auth.dart';
import 'dart:math' as math;

import 'package:storewarehouse/services/firebase_database.dart';

class AddTransaction extends StatefulWidget {
  @override
  _AddTransactionState createState() => _AddTransactionState();
}

class _AddTransactionState extends State<AddTransaction> {
  List<String> listOrders = [];
  List<ProductOrder> listProductOrders = [];
  List<ProductOrder> checkoutList = [];
  TextEditingController _costumerNameControler;
  // TextEditingController _searchProductController = TextEditingController();
  String date;
  int intDate;

  List<ProductOrder> filteredProductList = [];
  List<Product> productList;
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey();
  bool _isLoading = false;
  TextEditingController searchProductController = TextEditingController();
  String barcode = '';
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  int total = 0;
  PanelController _pc = PanelController();

  TextStyle style = TextStyle(
    color: Colors.white,
    fontSize: 15,
    fontWeight: FontWeight.bold,
  );

  getAllProduct() async {
    _isLoading = true;
    print('loading...');
    setState(() {});
    listProductOrders.clear();
    productList = await ProductProvider.getFastingByDone(false);
    print(productList.length);
    productList.forEach((prod) {
      ProductOrder newOrder = ProductOrder();
      newOrder.name = prod.name;
      newOrder.code = prod.code;
      newOrder.price = prod.price;
      newOrder.quantity = 0;
      newOrder.subtotal = 0;

      listProductOrders.add(newOrder);
    });
    // widget.updateCount(fasting.length);
    print('end loading...');
    _isLoading = false;
    setState(() {});
  }

  deleteAllProduct() async {
    await ProductProvider().deleteAll();
    getAllProduct();
  }

  searchProduct(String query) {
    _isLoading = true;
    setState(() {});
    print(productList.length);
    print(filteredProductList.length);
    print('okay');
    filteredProductList.clear();
    print(productList.length);
    print(filteredProductList.length);
    listProductOrders.forEach((productlistarray) {
      // print(productlistarray.name);
      if (productlistarray.code.toString().contains(query) ||
          productlistarray.name.toLowerCase().contains(query.toLowerCase())) {
        // print('ada');
        filteredProductList.add(productlistarray);
      }

      // if (productlistarray.name.toLowerCase().contains(query.toLowerCase())) {
      //   print('nomor');
      // }
    });

    print(productList.length);
    print(filteredProductList.length);
    _isLoading = false;
    setState(() {});
  }

  _showInfoSnackBar(message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      duration: Duration(milliseconds: 600),
      content: Text('Menambahkan $message'),
    ));
  }

  getDate() {
    DateTime now = DateTime.now();
    date = DateFormat('yyyyMMddkkmm').format(now);
    intDate = DateTime.now().millisecondsSinceEpoch ~/ 1000;
    print(intDate.toString());
    setState(() {});
  }

  bool isEditCostumer = false;

  @override
  void initState() {
    super.initState();
    getAllProduct();
    final now = DateTime.now();
    _costumerNameControler =
        TextEditingController(text: 'Costumer Pukul ' + now.hour.toString());

    getDate();
  }

  @override
  void dispose() {
    super.dispose();
  }

  countTotal() {
    total = 0;
    listProductOrders.forEach((single) {
      total += single.subtotal;
    });
  }

  @override
  Widget build(BuildContext context) {
    countTotal();

    FlutterMoneyFormatter fmf = new FlutterMoneyFormatter(
      amount: total.toDouble(),
      settings: MoneyFormatterSettings(
        symbol: 'Rp',
        thousandSeparator: '.',
        decimalSeparator: ',',
        symbolAndNumberSeparator: ' ',
        fractionDigits: 2,
        // compactFormatType: CompactFormatType.sort
      ),
    );

    return ModalProgressHUD(
      inAsyncCall: _isLoading,
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
        appBar: AppBar(
          title: Text('Buat Transaksi'),
          backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
        ),
        body: SlidingUpPanel(
          controller: _pc,
          backdropEnabled: true,
          maxHeight: MediaQuery.of(context).size.height * 0.78,
          minHeight: MediaQuery.of(context).size.height * 0.055,
          panelBuilder: (ScrollController sc) => _scrollingList(sc),
          // panel: Center(
          //   child: Text("This is the sliding Widget"),
          // ),
          collapsed: GestureDetector(
            onTap: () {
              _pc.open();
            },
            child: Container(
              // height: 20,
              color: Colors.blueGrey,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.add_shopping_cart,
                      color: Colors.white,
                    ),
                    SizedBox(width: 10),
                    Text(
                      "Tambah Belanjaan",
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),
          ),
          body: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  color: Color.fromRGBO(190, 190, 190, 1.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      if (isEditCostumer)
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            decoration: BoxDecoration(
                                color: Colors.white70,
                                borderRadius: BorderRadius.circular(10)),
                            child: TextField(
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  hintText: 'Nama Pelanggan'),
                            ),
                          ),
                        ),
                      // Text(intDate.toString()),
                      ListTile(
                        title: Text(_costumerNameControler.text),
                        subtitle: Text(date),
                        trailing: Text('${fmf.output.symbolOnLeft}'),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Consumer<Auth>(
                                builder: (BuildContext context, Auth auth,
                                        Widget child) =>
                                    RaisedButton(
                                  color: Color.fromRGBO(58, 66, 86, 1.0),
                                  child: Text(
                                    'Simpan',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  onPressed: () async {
                                    bool isSave = await showDialog(
                                        barrierDismissible: false,
                                        context: context,
                                        builder: (ctx) {
                                          return AlertDialog(
                                              title: Text('Simpan'),
                                              content: Text(
                                                  'Apakah data sudah benar?'),
                                              actions: [
                                                RaisedButton(
                                                  color: Color.fromRGBO(
                                                      58, 66, 86, 1.0),
                                                  child: Text('Ya'),
                                                  onPressed: () {
                                                    Navigator.pop(ctx, true);
                                                  },
                                                ),
                                                FlatButton(
                                                  onPressed: () {
                                                    Navigator.pop(ctx, false);
                                                  },
                                                  child: Text('Belum'),
                                                )
                                              ]);
                                        });
                                    if (!isSave || isSave == null) return;
                                    _isLoading = true;
                                    setState(() {});
                                    Orders newOrder = Orders(
                                      orderId: intDate.toString(),
                                      costumerName: _costumerNameControler.text
                                          .toString(),
                                      date: date,
                                      uid: auth.user.uid,
                                      total: total,
                                    );

                                    await FirestoreDatabase(uid: auth.user.uid)
                                        .createTransactionData(
                                            newOrder.toJson(), intDate);

                                    listProductOrders.forEach((single) async {
                                      if (single.quantity != 0) {
                                        await FirestoreDatabase(
                                                uid: auth.user.uid)
                                            .createTransactionDetailData(
                                                single.toJson(),
                                                intDate,
                                                single.code);
                                      }
                                    });

                                    _isLoading = false;
                                    setState(() {});
                                    Navigator.pop(context, true);
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.8,
                child: Divider(
                  color: Colors.white,
                ),
              ),
              Row(
                children: <Widget>[
                  Spacer(),
                  Icon(
                    Icons.shopping_cart,
                    color: Colors.white,
                  ),
                  SizedBox(width: 10),
                  Text(
                    'KERANJANG BELANJA',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  Spacer(),
                  RaisedButton(
                    padding: EdgeInsets.all(0),
                    child: Icon(Icons.add_shopping_cart),
                    onPressed: () {
                      _pc.open();
                    },
                  ),
                  Spacer(),
                ],
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.8,
                child: Divider(
                  color: Colors.white,
                ),
              ),
              Expanded(
                child: listProductOrders.length == 0
                    ? Container(
                        child: Center(
                          child: Text('Kosong'),
                        ),
                      )
                    : ListView.builder(
                        itemCount: listProductOrders.length,
                        itemBuilder: (BuildContext context, int i) {
                          FlutterMoneyFormatter fmf = new FlutterMoneyFormatter(
                            amount: listProductOrders[i].price.toDouble(),
                            settings: MoneyFormatterSettings(
                              symbol: 'Rp',
                              thousandSeparator: '.',
                              decimalSeparator: ',',
                              symbolAndNumberSeparator: ' ',
                              fractionDigits: 2,
                              // compactFormatType: CompactFormatType.sort
                            ),
                          );
                          return listProductOrders[i].quantity == 0
                              ? Container()
                              : Card(
                                  elevation: 5,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Color.fromRGBO(64, 75, 96, .9),
                                      // borderRadius: BorderRadius.circular(15),
                                      // border:
                                      //     Border.all(width: 2, color: Colors.grey)
                                    ),
                                    child: ListTile(
                                      title: Text(
                                        listProductOrders[i].name,
                                        style: style,
                                      ),
                                      subtitle: Text(
                                        ' ${listProductOrders[i].quantity} x  ${fmf.output.symbolOnLeft}',
                                        style: style,
                                      ),
                                      trailing: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Transform.rotate(
                                            angle: -math.pi / 360 * 90,
                                            child: IconButton(
                                              icon: Icon(
                                                Icons.highlight_off,
                                                color: Colors.white,
                                              ),
                                              onPressed: () {
                                                listProductOrders[i].quantity +=
                                                    1;
                                                listProductOrders[i].subtotal =
                                                    listProductOrders[i]
                                                            .quantity *
                                                        listProductOrders[i]
                                                            .price;
                                                setState(() {});
                                              },
                                            ),
                                          ),
                                          Text(
                                            listProductOrders[i]
                                                .quantity
                                                .toString(),
                                            style: style,
                                          ),
                                          IconButton(
                                            icon: Icon(
                                              Icons.do_not_disturb_on,
                                              color: Colors.white,
                                            ),
                                            onPressed: () {
                                              listProductOrders[i].quantity -=
                                                  1;

                                              listProductOrders[i].subtotal =
                                                  listProductOrders[i]
                                                          .quantity *
                                                      listProductOrders[i]
                                                          .price;
                                              setState(() {});
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                        }),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.055,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _scrollingList(ScrollController sc) {
    return Container(
      padding: EdgeInsets.all(10),
      color: Color(0xfff7f7f7),
      child: ListView.builder(
        controller: sc,
        itemCount: listProductOrders.length,
        itemBuilder: (BuildContext context, int i) {
          FlutterMoneyFormatter fmf = new FlutterMoneyFormatter(
            amount: listProductOrders[i].price.toDouble(),
            settings: MoneyFormatterSettings(
              symbol: 'Rp',
              thousandSeparator: '.',
              decimalSeparator: ',',
              symbolAndNumberSeparator: ' ',
              fractionDigits: 0,
              // compactFormatType: CompactFormatType.sort
            ),
          );

          return Card(
            elevation: 5,
            child: ListTile(
              title: Text(
                listProductOrders[i].name,
              ),
              subtitle: Text(fmf.output.symbolOnLeft.toString()),
              trailing: Container(
                decoration: BoxDecoration(
                  color: Color.fromRGBO(58, 66, 86, 1.0),
                  borderRadius: BorderRadius.circular(30),
                ),
                child: IconButton(
                  splashColor: Colors.grey,
                  icon: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    listProductOrders[i].quantity += 1;
                    listProductOrders[i].subtotal =
                        listProductOrders[i].quantity *
                            listProductOrders[i].price;
                    _showInfoSnackBar(listProductOrders[i].name);
                    setState(() {});
                  },
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
