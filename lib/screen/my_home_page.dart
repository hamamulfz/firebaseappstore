import 'package:device_info/device_info.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:storewarehouse/models/database_product.dart';
import 'package:storewarehouse/screen/nav/help_screen.dart';
import 'package:storewarehouse/screen/nav/list_screen.dart';
import 'package:storewarehouse/screen/nav/profile_screen.dart';
import 'package:storewarehouse/screen/nav/search_screen.dart';
import 'package:storewarehouse/screen/nav/transaction_screen.dart';
import 'package:storewarehouse/services/auth.dart';
import 'package:storewarehouse/services/firebase_database.dart';

import 'login_screen.dart';

class MyHomePage extends StatefulWidget {
  static const String route = 'home_screen';

  MyHomePage({
    Key key,
    // this.isFromLogin = false,
    this.title = 'Store Product',
  }) : super(key: key);
  // final bool isFromLogin;
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey();
  String barcode = '';

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  PageController _controller = PageController(
    initialPage: 0,
  );

  logOut(context) {
    Navigator.pushNamedAndRemoveUntil(
        context, LoginScreen.route, (route) => false);
  }

  getUser() async {
    return await FirebaseAuth.instance.currentUser();
  }

  _saveUid() async {
    final auth = await FirebaseAuth.instance.currentUser();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('user_uid', auth.uid);
  }

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  Future<String> getFcmToken() async {
    final token = await _firebaseMessaging.getToken();
    return token;
  }

  bool isFromLogin;
  getIsLoginAndSendNotif() async {
    final _prefs = await SharedPreferences.getInstance();
    isFromLogin = _prefs.getBool('isFromLogin');
    final user = await getUser();
    print('start from login ${isFromLogin}');
    if (isFromLogin) _sendSignInNotification(user);
  }

  isLogin() async {
    final _prefs = await SharedPreferences.getInstance();
    await _prefs.setBool('isFromLogin', false);

    print('end from login ${await _prefs.getBool('isFromLogin')}');
  }

  /// FLOW
  ///
  /// every user which sign in, will send data to firestore
  /// on create doc firestore, function will check the user
  /// if it first time login, it will noted as admin
  /// the second login will mark as foreign user
  /// so the admin user will get notified,
  /// it will promp to kick the second user of keep it
  _sendSignInNotification(FirebaseUser user) async {
    print('sending token notif');

    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    var token = await user.getIdToken();
    try {
      await FirestoreDatabase(uid: user.uid).createnewUserLoginNotif({
        'token': await getFcmToken(),
        "device": androidInfo.model,
        "token_id": token.token,
      });
      await FirestoreDatabase(uid: user.uid).createNewUserInfo({
        'token': await getFcmToken(),
        "device": androidInfo.model,
        "token_id": token.token,
      });
      await isLogin();
    } catch (r) {
      print('error here');
      print(r);
    }
  }

  /// once admin user get notified,
  /// if it choose to log out the user, send to firestore logout path
  forceLogOut(FirebaseUser user, String token) async {
    await FirestoreDatabase(uid: user.uid).createnewUserLogoutNotif({
      'token': token,
      'admin': await getFcmToken(),
    });
  }

  /// the second user is forced to log out
  /// give notification to admin user
  successLogOut(FirebaseUser user, String token) async {
    await FirestoreDatabase(uid: user.uid).createLogoutSuccessNotif({
      'token': token,
    });
  }

  getSubscribe() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    bool isSubscribe = _prefs.getBool('isSubscribe') ?? true;
    if (isSubscribe) _firebaseMessaging.subscribeToTopic('helps');
  }

  fcmAction(message, auth) async {
    print("onMessage: $message");
    // _showItemDialog(message);
    // isShowNotif = true;
    if ("Keluar Paksa" == message['notification']['title']) {
      await successLogOut(auth.user, message['data']['admin']);
      await auth.signOut();
      _firebaseMessaging.unsubscribeFromTopic('helps');
      Navigator.of(context).pushNamedAndRemoveUntil(
          LoginScreen.route, (Route<dynamic> route) => false);
      // _scaffoldKey.currentState.showSnackBar(SnackBar(
      //   duration: Duration(seconds: 5),
      //   content: Text('Anda di keluarkan oleh pemilik akun.'),
      // ));
    }

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => AlertDialog(
        title: Text(message['notification']['title']),
        content: ListTile(
          title: Text(message['notification']['body']),
        ),
        actions: <Widget>[
          if ("Terdeteksi Login Perangkat Baru" ==
              message['notification']['title'])
            FlatButton(
              color: Colors.green,
              child: Text(
                'Force Log Out',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () async {
                await forceLogOut(auth.user, message['data']['token']);
                Navigator.of(context).pop();
              },
            ),
          FlatButton(
              child: Text('Ok'),
              onPressed: () {
                if ("Permintaan Bantuan" == message['notification']['title'])
                  _onItemTapped(2);
                Navigator.of(context).pop();
              }),
        ],
      ),
    );
    setState(() {});
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _controller.dispose();
  }

  @override
  void initState() {
    super.initState();
    final auth = Provider.of<Auth>(context, listen: false);
    auth.checkUser();
    _saveUid();

    if (auth.user == null) {
      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text('Please Sign In Again'),
              content: Text('No user detected. Please Sign In Again'),
              actions: <Widget>[
                RaisedButton(
                  child: Text('OK'),
                  onPressed: () {
                    logOut(context);
                  },
                )
              ],
            );
          });
      return;
    }
    auth.setIsLoginValue(false);
    getIsLoginAndSendNotif();
    getSubscribe();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        fcmAction(message, auth);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");

        fcmAction(message, auth);
        if ("Permintaan Bantuan" == message['notification']['title'])
          _onItemTapped(2);
        // _navigateToItemDetail(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");

        fcmAction(message, auth);
        // _navigateToItemDetail(message);
        if ("Permintaan Bantuan" == message['notification']['title'])
          _onItemTapped(2);
      },
    );

    // _firebaseMessaging.requestNotificationPermissions(
    //     const IosNotificationSettings(
    //         sound: true, badge: true, alert: true, provisional: true));
    // _firebaseMessaging.onIosSettingsRegistered
    //     .listen((IosNotificationSettings settings) {
    //   print("Settings registered: $settings");
    // });
    // _firebaseMessaging.getToken().then((String token) {
    //   assert(token != null);
    //   setState(() {
    //     var _homeScreenText = "Push Messaging token: $token";
    //     print(_homeScreenText);
    //   });
    // });
  }

  int _selectedIndex = 0;
  // static const TextStyle optionStyle =
  //     TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      _controller.animateToPage(
        _selectedIndex,
        duration: new Duration(milliseconds: 100),
        curve: Curves.easeIn,
      );
    });
  }

  changeSelected(number, a, b) {
    return number == _selectedIndex ? a : b;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      key: _scaffoldKey,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor:
            changeSelected(2, Colors.yellow, Color.fromRGBO(58, 66, 86, 1.0)),
        child: Icon(
          Icons.attach_money,
          color:
              changeSelected(2, Color.fromRGBO(58, 66, 86, 1.0), Colors.white),
        ),
        onPressed: () {
          _onItemTapped(2);
        },
      ),
      bottomNavigationBar: Container(
        height: 55.0,
        child: BottomAppBar(
          shape: CircularNotchedRectangle(),
          color: Color.fromRGBO(58, 66, 86, 1.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              IconButton(
                tooltip: 'Produk Saya',
                icon: Icon(
                  Icons.home,
                  color: changeSelected(0, Colors.yellow, Colors.white),
                  size: changeSelected(0, 30.0, 20.0),
                ),
                onPressed: () {
                  _onItemTapped(0);
                },
              ),
              IconButton(
                tooltip: 'Cari Barang',
                icon: Icon(
                  Icons.blur_on,
                  color: changeSelected(1, Colors.yellow, Colors.white),
                  size: changeSelected(1, 30.0, 20.0),
                ),
                onPressed: () {
                  _onItemTapped(1);
                },
              ),
              IconButton(
                tooltip: 'Transaksi',
                icon: Icon(
                  Icons.attach_money,
                  color:
                      changeSelected(2, Colors.transparent, Colors.transparent),
                  size: changeSelected(2, 30.0, 20.0),
                ),
                onPressed: () {
                  _onItemTapped(2);
                },
              ),
              IconButton(
                tooltip: 'Minta Bantuan',
                icon: Icon(
                  Icons.live_help,
                  color: changeSelected(3, Colors.yellow, Colors.white),
                  size: changeSelected(3, 30.0, 20.0),
                ),
                onPressed: () {
                  _onItemTapped(3);
                },
              ),
              AnimatedContainer(
                child: IconButton(
                  tooltip: 'Profil saya',
                  icon: Icon(
                    Icons.account_box,
                    color: changeSelected(4, Colors.yellow, Colors.white),
                    size: changeSelected(4, 30.0, 20.0),
                  ),
                  onPressed: () {
                    _onItemTapped(4);
                  },
                ),
                duration: Duration(seconds: 1),
              )
            ],
          ),
        ),
      ),
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: _controller,
        children: <Widget>[
          ListScreen(),
          SearchScreen(),
          TransactionScreen(),
          HelpScreen(),
          ProfileScreen(logout: logOut),
        ],
      ),
    );
  }
}
