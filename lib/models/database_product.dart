import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart';

final String tableProductName = 'product';
final String columnId = 'id';
final String columnCode = 'code';
final String columnName = 'name';
final String columnPrice = 'price';
final String columnStock = 'stock';
final String columnCreatedBy = 'create_by';
final String columnCreateAt = 'create_at';
final String columnUpdateAt = 'update_at';

// {
//   'code': "$1",
//   'name': "$1",
//   'price': 0,
//   'stock': 0,
//   'create_at': 1584506024,
//   'update_at': 1584506024,
// }

class Product {
  int id;
  String code;
  String name;
  int stock;
  int price;
  String createBy;
  int createAt;
  int updateAt;

  Product({
    this.code,
    this.name,
    this.price,
  });

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnCode: code,
      columnName: name,
      columnPrice: price,
      columnStock: stock,
      columnStock: stock,
      columnCreatedBy: createBy,
      columnCreateAt: updateAt,
      columnUpdateAt: createAt,
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }

  Product.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    code = map[columnCode];
    name = map[columnName];
    stock = map[columnStock];
    price = map[columnPrice];
    createBy = map[columnCreatedBy];
    updateAt = map[columnUpdateAt];
    createAt = map[columnCreateAt];
  }
}

class ProductProvider {
  static Future<Database> database() async {
    final dbPath = await getDatabasesPath();

    return openDatabase(path.join(dbPath, '$tableProductName.db'),
        onCreate: (db, version) {
      return db.execute('''

          create table $tableProductName ( 
            $columnId integer primary key autoincrement, 
            $columnCode text not null unique,
            $columnName text not null,
            $columnPrice integer not null default 0,
            $columnStock integer default 0,
            $columnCreatedBy text  null,
            $columnUpdateAt integer,
            $columnCreateAt integer )
          ''');
    }, version: 2);
  }

  static Future<void> insert(Product fasting) async {
    final db = await ProductProvider.database();

    int myTimeStamp = DateTime.now().millisecondsSinceEpoch;
    fasting.updateAt = myTimeStamp;
    fasting.createAt = myTimeStamp;
    fasting.createBy = 'null';

    db.insert(
      tableProductName,
      fasting.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  static Future<List<Product>> getData() async {
    final db = await ProductProvider.database();
    List<Map> maps = await db.query(tableProductName);
    List<Product> fasting = [];

    if (maps.length > 0) {
      maps.forEach((data) {
        fasting.add(Product.fromMap(data));
      });
    }
    return fasting;
  }

  getAllData(done) async {
    final db = await ProductProvider.database();

    var res = await db.query(
      tableProductName,
      where: '$columnPrice = ?',
      whereArgs: [done ? 1 : 0],
    );
    List<Product> list =
        res.isNotEmpty ? res.map((c) => Product.fromMap(c)).toList() : [];
    return list;
  }

  static getFastingByDone(bool done) async {
    final db = await ProductProvider.database();
    List<Map> maps = await db.query(
      tableProductName, orderBy: columnName,
      // columns: [
      //   columnId,
      //   columnCode,
      //   columnName,
      //   columnPrice,
      // ],
      // where: '$columnPrice = ?',
      // whereArgs: [done == true ? 1 : 0],
    );
    print(maps);
    List<Product> product = [];

    if (maps.length > 0) {
      maps.forEach((data) {
        product.add(Product.fromMap(data));
      });
    }
    return product;
  }

  Future<int> delete(int id) async {
    final db = await ProductProvider.database();

    return await db
        .delete(tableProductName, where: '$columnId = ?', whereArgs: [id]);
  }

  deleteAll() async {
    final db = await ProductProvider.database();

    // db.rawDelete("delete from $tableProductName");
    await db.delete(tableProductName);
  }

  Future<int> update(Product product) async {
    int myTimeStamp = DateTime.now().millisecondsSinceEpoch;
    product.updateAt = myTimeStamp;
    final db = await ProductProvider.database();

    return await db.update(
      tableProductName,
      product.toMap(),
      where: '$columnId = ?',
      whereArgs: [product.id],
    );
  }

  Future close() async {
    final db = await ProductProvider.database();
    db.close();
  }
}
