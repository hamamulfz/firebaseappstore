var json = {
  "order_id": "id",
  "costumer_name": "id",
  "date": "id",
  "uid": "id",
  "total": "id",
};

var prod = {
  "product": [
    {
      "code": "id",
      "name": "id",
      "price": 1,
      "quantity": 1,
      "subtotal": 1,
    }
  ]
};

class Orders {
  String orderId;
  String costumerName;
  String date;
  String uid;
  int total;

  Orders({this.orderId, this.costumerName, this.date, this.uid, this.total});

  Orders.fromJson(Map<String, dynamic> json) {
    orderId = json['order_id'];
    costumerName = json['costumer_name'];
    date = json['date'];
    uid = json['uid'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['order_id'] = this.orderId;
    data['costumer_name'] = this.costumerName;
    data['date'] = this.date;
    data['uid'] = this.uid;
    data['total'] = this.total;
    return data;
  }
}

class TxOrderDetail {
  List<ProductOrder> product;

  TxOrderDetail({this.product});

  TxOrderDetail.fromJson(Map<String, dynamic> json) {
    if (json['product'] != null) {
      product = new List<ProductOrder>();
      json['product'].forEach((v) {
        product.add(new ProductOrder.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.product != null) {
      data['product'] = this.product.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ProductOrder {
  String code;
  String name;
  int price;
  int quantity;
  int subtotal;

  ProductOrder(
      {this.code, this.name, this.price, this.quantity, this.subtotal});

  ProductOrder.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    name = json['name'];
    price = json['price'];
    quantity = json['quantity'];
    subtotal = json['subtotal'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['name'] = this.name;
    data['price'] = this.price;
    data['quantity'] = this.quantity;
    data['subtotal'] = this.subtotal;
    return data;
  }
}
