import 'package:flutter/widgets.dart';

class UserData {
  UserData({
    @required this.name,
    @required this.email,
    @required this.phone,
    @required this.uid,
    @required this.fcmToken,
    @required this.deviceId,
    @required this.photoUrl,
  });
  final String name;
  final String email;
  final String phone;
  final String uid;
  final String fcmToken;
  final String deviceId;
  final String photoUrl;

  Map<String, dynamic> toJson() {
    return {
      "name": name,
      "email": email,
      "phone": phone,
      "uid": uid,
      "fcm_token": fcmToken,
      "device_id": deviceId,
    };
  }
}
