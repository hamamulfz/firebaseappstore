// import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
// import 'package:flutter_firebase/models/jobs.dart';
// import 'package:flutter_firebase/sevices/api_path.dart';
import 'package:storewarehouse/services/api_path.dart';

class FirestoreDatabase extends ChangeNotifier {
  FirestoreDatabase({
    @required this.uid,
  }) : assert(uid != null);
  String uid;

  Future<void> _setData({path, Map<String, dynamic> data}) async {
    final documentReference = Firestore.instance.document(path);
    await documentReference.setData(data);
  }

  Future<void> _pushData({path, Map<String, dynamic> data}) async {
    final docReferee = Firestore.instance.collection(path);
    print(docReferee.id);
    data['fsid'] = docReferee.id;
    await docReferee.add(data);
  }

  Future<void> createUserData(Map<String, dynamic> job) async {
    Firestore.instance.collection('users').document().setData({
      // 'name': displayName,
      // 'uid': user.uid,
      // 'email': user.email,
      // 'isEmailVerified': user.isEmailVerified, // will also be false
      // 'photoUrl': user.photoUrl, // will always be null
    });
    await _setData(
      path: FbfsPath.userData(uid),
      data: job,
    );
  }

  void readUserData() {
    final path = FbfsPath.userData(uid);
    final reference = Firestore.instance.collection(path);
    final snapshot = reference.snapshots();
    snapshot.listen((snapshot) {
      snapshot.documents.forEach((f) {
        print(f.data);
      });
    });
  }

  Future<void> createUserProductData(Map<String, dynamic> job, pathId) async =>
      await _setData(
        path: FbfsPath.userProduct(uid, pathId),
        data: job,
      );
  // DateTime.now().millisecondsSinceEpoch.toString()

  Future<void> createPublicProductData(Map<String, dynamic> job, code) async =>
      await _setData(
        path: FbfsPath.publicProduct(code),
        data: job,
      );

  Future<void> createTransactionData(Map<String, dynamic> job, orderId) async =>
      await _setData(
        path: FbfsPath.transactions(uid, orderId),
        data: job,
      );
  Future<void> createTransactionDetailData(
          Map<String, dynamic> job, orderId, code) async =>
      await _setData(
        path: FbfsPath.transactionsDetails(uid, orderId, code),
        data: job,
      );
  Future<void> createPublicTxData(Map<String, dynamic> job, orderId) async =>
      await _setData(
        path: FbfsPath.publicOrders(orderId),
        data: job,
      );
  Future<void> createPublicTxDetailData(
          Map<String, dynamic> job, orderId, code) async =>
      await _setData(
        path: FbfsPath.pulicOrderDetails(orderId, code),
        data: job,
      );

  Future<void> testNotifikasi(Map<String, dynamic> job) async {
    var ts = DateTime.now().millisecondsSinceEpoch.toString();
    await _setData(
      path: 'test-notif/$uid-$ts',
      data: job,
    );
  }

  Future<void> createnewUserLoginNotif(Map<String, dynamic> job) async {
    var ts = DateTime.now().millisecondsSinceEpoch.toString();
    job['uid'] = uid.toString();
    await _setData(
      path: 'login/$uid-$ts',
      data: job,
    );
  }

  Future<void> createnewUserLogoutNotif(Map<String, dynamic> job) async {
    var ts = DateTime.now().millisecondsSinceEpoch.toString();
    job['uid'] = uid.toString();
    await _setData(
      path: 'logout/$uid-$ts',
      data: job,
    );
  }

  Future<void> createLogoutSuccessNotif(Map<String, dynamic> job) async {
    var ts = DateTime.now().millisecondsSinceEpoch.toString();
    job['uid'] = uid.toString();
    await _setData(
      path: 'logout-success/$uid-$ts',
      data: job,
    );
  }

  Future<void> createNewUserInfo(Map<String, dynamic> job) async {
    var ts = DateTime.now().millisecondsSinceEpoch.toString();
    job['uid'] = uid.toString();
    await _setData(
      path: 'users/$uid/login/$uid-$ts',
      data: job,
    );
  }

  // void readPublicProductData() {
  //   final path = FbfsPath.userProduct(
  //       uid, DateTime.now().millisecondsSinceEpoch.toString());
  //   final reference = Firestore.instance.collection(path);
  //   final snapshot = reference.getDocuments().then((snapshot) {
  //     snapshot.documents.forEach((f) => print('${f.data}}'));
  //   });
  //   // return snapshot;
  // }

  Future<void> createHelp(Map<String, dynamic> job) async => await _setData(
        path: FbfsPath.help(
            uid, DateTime.now().millisecondsSinceEpoch.toString()),
        data: job,
      );

  Future<QuerySnapshot> readHelp() {
    final path = FbfsPath.helpsCollection();
    final reference = Firestore.instance.collection(path);
    final snapshot = reference.getDocuments();
    // snapshot.then((snap) {
    //   snap.documents.forEach((f) => print('${f.data}}'));
    // });
    return snapshot;
    // .then((snapshot) {
    //   snapshot.documents.forEach((f) => print('${f.data}}'));
    // });
    // return snapshot;
  }

  Future<QuerySnapshot> readUserProductData() {
    final path = FbfsPath.userProductCollection(uid);
    final reference = Firestore.instance.collection(path);

    final snapshot = reference.getDocuments();
    // reference.id;
    // final snapshot = reference.getDocuments().then((snapshot) {
    //   snapshot.documents.forEach((f) => print('${f.data}}'));
    // });
    return snapshot;
  }

  Future<QuerySnapshot> readOrders(uid) {
    final path = FbfsPath.transactionCollection(uid);
    final reference = Firestore.instance.collection(path);
    final snapshot = reference.getDocuments();
    snapshot.then((snap) {
      snap.documents.forEach((f) => print('${f.data}}'));
    });
    return snapshot;
    // .then((snapshot) {
    //   snapshot.documents.forEach((f) => print('${f.data}}'));
    // });
    // return snapshot;
  }

  Future<QuerySnapshot> readOrderDetails(uid, id) {
    final path = FbfsPath.transactionDetailCollection(uid, id);
    final reference = Firestore.instance.collection(path);
    final snapshot = reference.getDocuments();
    snapshot.then((snap) {
      snap.documents.forEach((f) => print('${f.data}}'));
    });
    return snapshot;
    // .then((snapshot) {
    //   snapshot.documents.forEach((f) => print('${f.data}}'));
    // });
    // return snapshot;
  }

  Future<QuerySnapshot> readPublicProduct() async {
    final path = FbfsPath.publicProductCollection();
    final reference = Firestore.instance.collection(path);
    final snapshot = await reference.getDocuments();
    // snapshot.then((snap) {
    //   snap.documents.forEach((f) => print('${f.data}}'));
    // });
    return snapshot;
    // .then((snapshot) {
    //   snapshot.documents.forEach((f) => print('${f.data}}'));
    // });
    // return snapshot;
  }

  void updateData(code) {
    try {
      final path = FbfsPath.help(uid, code);
      final reference = Firestore.instance.collection(path);
      reference.document('1').updateData({'description': 'Head First Flutter'});
    } catch (e) {
      print(e.toString());
    }
  }

  void deleteData(code) {
    try {
      final path = FbfsPath.help(uid, code);
      final reference = Firestore.instance.collection(path);
      reference.document('1').delete();
    } catch (e) {
      print(e.toString());
    }
  }
}
