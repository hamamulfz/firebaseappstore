class Value {
//shared pref
  static const String fcmToken = 'fcm_token';
  static const String userToken = 'user_token';
  static const String userUid = 'user_uid';
  static const String isFirstLauch = 'is_first_launch';

  static const String userName = 'username';
  static const String userPhone = 'user_phone';
  static const String userEmail = 'user_email';
}
