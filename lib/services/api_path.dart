class FbfsPath {
  //document
  static String userData(uid) => "users/$uid";
  static String userDevice(uid, device) => "users/$uid/device/$device";
  static String userSharing(uid, otherUid) => "users/$uid/share/$otherUid";
  static String publicProduct(code) => "public/$code";
  static String userProduct(uid, code) => "product/user/$uid/$code";
  static String help(uid, name) => "helps/$uid-$name";
  static String share(uid, otherUid) => "share/$uid/person/$otherUid";

  //transaction document for user
  static String transactions(uid, order) => "tx/$uid/order/$order";
  static String transactionsDetails(uid, order, code) =>
      "tx/$uid/order/$order/details/$code";

  //transaction document for user
  static String publicOrders(orderId) => "ptx/$orderId";
  static String pulicOrderDetails(orderId, code) =>
      "ptx/$orderId/details/$code";

  //collection
  static String userProductCollection(uid) => "product/user/$uid";
  static String helpsCollection() => "helps";
  static String publicProductCollection() => "public";

  static String transactionCollection(uid) => "tx/$uid/order";
  static String transactionDetailCollection(uid, id) =>
      "tx/$uid/order/$id/details";

  static String publicTxCollection() => "ptx";
  static String publicTxDetailCollection(orderId) => "ptx/$orderId/details";

  static String publicOrdersCollection(orderId) => "ptx";
  static String pulicOrderDetailsCollection(orderId, code) =>
      "ptx/$orderId/details";
}
