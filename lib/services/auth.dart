import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:flutter_twitter/flutter_twitter.dart';
// import 'package:flutter_twitter/flutter_twitter.dart';
// import 'package:flutter_firebase/bloc/sign_in_bloc.dart';
// import 'package:flutter_twitter_login/flutter_twitter_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
// import 'package:url_launcher/url_launcher.dart';
// import 'package:uni_links/uni_links.dart';

// import '';

class User {
  User({this.uid});
  final String uid;
}

class Auth extends ChangeNotifier {
  bool _isLoadingAuth = false;
  bool _isInLoginPage = false;

  bool get isLoadingAuth => _isLoadingAuth;
  bool get isInLoginPage => _isInLoginPage;

  setLoadingValue(bool val) {
    _isLoadingAuth = val;
    notifyListeners();
  }

  setIsLoginValue(bool val) {
    _isInLoginPage = val;
    print('is login page: ' + _isInLoginPage.toString());
    if (this._user != null) {
      print(user.displayName);
    }
    // notifyListeners();
  }

  FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  String _smsVerificationCode = "";
  FirebaseUser _user;

  FirebaseUser get user => _user;
  FirebaseUser setUser(user) => this._user = user;

  checkUser() async {
    _user = await _firebaseAuth.currentUser();
    notifyListeners();
    return _user;
  }

  _resultFromAuth(FirebaseUser user) {
    if (user == null) {
      this._user = null;
      return null;
    }
    this._user = user;
    return User(uid: user.uid);
  }

  String get smsCode {
    return _smsVerificationCode;
  }

  Stream<User> get onAuthStateChange {
    return _firebaseAuth.onAuthStateChanged
        .map((user) => _resultFromAuth(user));
  }

  Future<FirebaseUser> signInWithEmailAndPassword(
      String email, String password) async {
    FirebaseUser user = (await _firebaseAuth.signInWithEmailAndPassword(
            email: email, password: password))
        .user;
    this._user = user;
    return user;
  }

  verifyPhoneNumber(
      BuildContext context, String countryCode, String number) async {
    setLoadingValue(true);
    print(countryCode + number);
    _firebaseAuth.setLanguageCode('id');
    String phoneNumber = countryCode + number.toString();
    print(phoneNumber);
    await _firebaseAuth.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        timeout: Duration(seconds: 10),
        verificationCompleted: (authCredential) =>
            _verificationComplete(authCredential, context),
        verificationFailed: (authException) =>
            _verificationFailed(authException, context),
        codeAutoRetrievalTimeout: (verificationId) =>
            _codeAutoRetrievalTimeout(verificationId),
        // called when the SMS code is sent
        codeSent: (verificationId, [code]) =>
            _smsCodeSent(verificationId, [code]));

    setLoadingValue(false);
  }

  senOtpManually(String code) async {
    final AuthCredential credential = PhoneAuthProvider.getCredential(
        smsCode: code, verificationId: _smsVerificationCode);
    final FirebaseUser user =
        (await _firebaseAuth.signInWithCredential(credential)).user;
    this._user = user;
    notifyListeners();
    return user;
  }

  _verificationComplete(AuthCredential authCredential, BuildContext context) {
    FirebaseAuth.instance
        .signInWithCredential(authCredential)
        .then((authResult) {
      // final snackBar =
      //     SnackBar(content: Text("Success!!! UUID is: " + authResult.user.uid));
      this._user = authResult.user;
      notifyListeners();
      // Navigator.pushReplacementNamed(context, MyHomePage.route);

      // Scaffold.of(context).showSnackBar(snackBar);
      print("Completed");
      return authResult.user;
    });
  }

  _smsCodeSent(String verificationId, List<int> code) {
    // set the verification code so that we can use it to log the user in
    _smsVerificationCode = verificationId;
    print("Send + $verificationId");
  }

  _verificationFailed(AuthException authException, BuildContext context) {
    print("Failed");
    // final snackBar = SnackBar(
    //     content:
    //         Text("Exception!! message:" + authException.message.toString()));
    // Scaffold.of(context).showSnackBar(snackBar);
  }

  _codeAutoRetrievalTimeout(String verificationId) {
    print("Time out");
    // set the verification code so that we can use it to log the user in
    _smsVerificationCode = verificationId;
  }

  Future<FirebaseUser> registerWithEmailAndPassword(
      String email, String password) async {
    FirebaseUser user = (await _firebaseAuth.createUserWithEmailAndPassword(
            email: email, password: password))
        .user;

    // await user.sendEmailVerification();
    notifyListeners();
    return user;
  }

  updateUserInfo(FirebaseUser user, {String name, String url}) {
    UserUpdateInfo updateUserInfo = UserUpdateInfo();
    if (name != null) updateUserInfo.displayName = name;
    if (url != null) updateUserInfo.photoUrl = url;
    user.updateProfile(updateUserInfo);
  }

  Future<void> resetPassword(String email) async {
    await _firebaseAuth.sendPasswordResetEmail(email: email);
  }

  Future<void> updateProfile(String email) async {
    await _user.updateProfile(UserUpdateInfo());
  }

  // Future<User> checkUser() async {
  //   FirebaseUser user = await _firebaseAuth.currentUser();
  //   return _resultFromAuth(user);
  // }

  Future<User> signInAnonymously() async {
    FirebaseUser user = (await _firebaseAuth.signInAnonymously()).user;
    return _resultFromAuth(user);
  }

  var twitterLogin = new TwitterLogin(
    consumerKey: 'FbeVAqVO39Lzf7IoEilaJjmsf',
    consumerSecret: '7JqIk101uBbkLmXJMlJdJX3jnowOMPNd21T0TkKkuMXh1WgyOa',
  );
  Future signOut() async {
    setLoadingValue(true);
    notifyListeners();
    final googleSignin = GoogleSignIn();
    final facebookLogin = FacebookLogin();
    await googleSignin.signOut().catchError((error) {
      print(error.toString());
    });
    print('google is finish');
    await facebookLogin.logOut().catchError((error) {
      print(error.toString());
    });
    print('fb is finish');
    await twitterLogin.logOut();
    await _firebaseAuth.signOut().then((onValue) {
      print('success out');
    }).catchError((error) {
      print(error.toString());
    });
    this._user = null;

    setLoadingValue(false);
    notifyListeners();
  }

  Future<FirebaseUser> signInWithTwitter() async {
    TwitterLoginResult result;
    try {
      result = await twitterLogin.authorize();

      print('status ${result.status}');
    } catch (e) {
      print(e);
    }

    switch (result.status) {
      case TwitterLoginStatus.loggedIn:
        var session = result.session;
        print('Yeay, session: ${session.token} secret: ${session.secret}');
        final AuthCredential credential = TwitterAuthProvider.getCredential(
            authToken: session.token, authTokenSecret: session.secret);
        FirebaseUser user =
            (await _firebaseAuth.signInWithCredential(credential)).user;
        print("twitter sign in" + user.toString());
        setLoadingValue(false);
        this._user = user;
        notifyListeners();

        return user;
        break;
      case TwitterLoginStatus.cancelledByUser:
        print("Why did you cancelled the sign in?");

        return null;
        break;
      case TwitterLoginStatus.error:
        print('Oh Error: ' + result.errorMessage);
        setLoadingValue(false);

        return null;
        break;

      default:
        setLoadingValue(false);
        return null;
        break;
    }
  }

  Future<FirebaseUser> signInWithFacebook() async {
    final facebookLogin = FacebookLogin();
    facebookLogin.loginBehavior = FacebookLoginBehavior.webViewOnly;
    final FacebookLoginResult result = await facebookLogin.logIn(['email']);

    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        AuthCredential credential = FacebookAuthProvider.getCredential(
          accessToken: result.accessToken.token,
        );

        final FirebaseUser user =
            (await _firebaseAuth.signInWithCredential(credential)).user;

        this._user = user;
        // notifyListeners();

        return user;
        break;
      case FacebookLoginStatus.cancelledByUser:
        setLoadingValue(false);

        return null;
        break;
      case FacebookLoginStatus.error:
        setLoadingValue(false);
        return null;
        break;
      default:
        setLoadingValue(false);
        return null;
        break;
    }
  }

  Future<FirebaseUser> signInWithGoogle() async {
    GoogleSignIn googleSignIn = GoogleSignIn();
    GoogleSignInAccount googleUser;
    try {
      googleUser = await googleSignIn.signIn();
      print("User is $googleUser.");
    } catch (e, s) {
      print('Exception handled.');
      print(e);
      print(s);
    }

    if (googleUser != null) {
      GoogleSignInAuthentication googleAuth = await googleUser.authentication;
      if (googleAuth.idToken != null && googleAuth.accessToken != null) {
        AuthCredential idGoogle = GoogleAuthProvider.getCredential(
          idToken: googleAuth.idToken,
          accessToken: googleAuth.accessToken,
        );
        final FirebaseUser userGoogle =
            (await _firebaseAuth.signInWithCredential(idGoogle)).user;
        this._user = userGoogle;
        // notifyListeners();

        return userGoogle;
      } else {
        setLoadingValue(false);
        throw PlatformException(
            code: 'ERROR_MISSING_GOOGL_AUTH_TOKEN',
            message: 'Missing google auth token');
      }
    } else {
      setLoadingValue(false);
      throw PlatformException(
          code: 'ERROR_ABORTED_BY_USER', message: 'aborted by user');
    }
  }
}
