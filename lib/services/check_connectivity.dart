import 'package:connectivity/connectivity.dart';

class CheckConnectivity {
  static Future<bool> checkConnectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());

    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      print("Internet is available");
      return true;
    } else {
      print("Internet is not available");

      return false;
    }
  }
}
