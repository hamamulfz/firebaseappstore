import 'dart:async';

import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:storewarehouse/screen/login_screen.dart';
import 'package:storewarehouse/screen/my_home_page.dart';
import 'package:storewarehouse/screen/new/welcome_page.dart';
import 'package:storewarehouse/screen/scan_qr_page.dart';
import 'package:storewarehouse/screen/splash_screen.dart';
import 'package:storewarehouse/services/auth.dart';

// import 'translations.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  Crashlytics.instance.enableInDevMode = true;

  LicenseRegistry.addLicense(() async* {
    final license = await rootBundle.loadString('google_fonts/OFL.txt');
    yield LicenseEntryWithLineBreaks(['google_fonts'], license);
  });
  // Pass all uncaught errors from the framework to Crashlytics.
  FlutterError.onError = Crashlytics.instance.recordFlutterError;

  runZoned(() {
    runApp(MyApp());
  }, onError: Crashlytics.instance.recordError);
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          // fontFamily: "Poppins",
          textTheme: GoogleFonts.poppinsTextTheme(
            Theme.of(context).textTheme,
          ),
          primarySwatch: Colors.blue,
          backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
        ),
        home: WelcomePage(),
        // onGenerateRoute: (settings) {
        //   final arguments = settings.arguments;
        //   switch (settings.name) {
        //     case MyHomePage.route:
        //       if (arguments is bool) {
        //         // the details page for one specific user
        //         return MaterialPageRoute(
        //             builder: (context) => MyHomePage(isFromLogin: true));
        //       } else {
        //         return MaterialPageRoute(builder: (context) => MyHomePage());
        //       }
        //       break;
        //     default:
        //       return null;
        //   }
        // },
        routes: {
          SplashScreen.route: (context) => SplashScreen(),
          LoginScreen.route: (context) => LoginScreen(),
          MyHomePage.route: (context) => MyHomePage(),
          // EditProfileScreen.route: (context) => EditProfileScreen(),
          ScanQrPage.route: (context) => ScanQrPage()
        },
      ),
      providers: [
        ChangeNotifierProvider<Auth>(create: (_) => Auth()),
      ],
    );
  }
}
